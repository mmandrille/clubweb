from django.conf.urls import url
from django.urls import path
#Import de modulos personales
from . import views

app_name = 'noticias'
urlpatterns = [
    path('', views.ver_noticias, name='noticias'),
    path('<int:id_noticia>', views.ver_noticia, name='noticia'),
    path('carousel', views.carousel, name='carousel'),

    path('buscar', views.buscar_noticias, name='buscar_noticias'),
    path('tags/<int:id_tag>', views.buscar_etiqueta, name='buscar_etiqueta'),

]