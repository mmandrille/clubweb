#Imports Django
from django import template
#Imports del proyecto
from noticias.models import Noticia

register = template.Library()

@register.simple_tag
def get_etiquetas():
    print('hola')
    return Noticia.etiquetas.most_common()[:5]