from __future__ import unicode_literals
#Imports de Python
from datetime import date
#Imports de Django
from django.db import models
#Imports de modulos Extra
from tinymce.models import HTMLField

# Create your models here.
class Unidad(models.Model):
    padre = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True, related_name='hijos')
    nombre = models.CharField('Titulo', max_length=200)
    descripcion = HTMLField(blank=True, null=True)
    icono = models.FileField('Icono', upload_to='organigrama/icono/', blank=True, null=True)
    direccion = models.CharField('Direccion', max_length=200, blank=True, null=True)
    cuit = models.CharField('CUIT', max_length=13, blank=True, null=True)
    telefonos = models.CharField('Telefonos', max_length=100, blank=True, null=True)
    color = models.CharField(max_length=7, default="#ffffff")
    activo = models.BooleanField(default=True)
    visible = models.BooleanField(default=True)
    primario = models.BooleanField(default=False)
    class Meta:
        verbose_name = 'Unidad Organizativa'
        verbose_name_plural = 'Unidades Organizativas'
    def __str__(self):
        if self.padre is not None:
            return self.nombre + ' > ' + self.padre.nombre
        else:
            return self.nombre + ' (Sin Asignar)'
    def as_dict(self):
        if self.padre is None:
            self.padre = self
        return {
            "id": self.id,
            "nombre": self.nombre,
            "padre": self.padre.id,
        }
    def as_full_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
            "descripcion": self.descripcion,
            "direccion": self.direccion,
            "telefonos": self.telefonos,
            "web": self.web,
        }
    def empleados_activos(self):
        empleados = []
        try:
            for contrato in self.contratos.filter(endda=date(9999, 12, 31)):
                empleados.append(contrato.empleado)
        except:
            pass
        return empleados
    def save(self, *args, **kwargs):
        if self.primario == True:
            Unidad.objects.all().exclude(pk=self.id).update(primario=False)
        super(Unidad, self).save(*args, **kwargs)