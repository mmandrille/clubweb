from django.forms import ModelForm
from django.forms.widgets import TextInput
from .models import Unidad

class UnidadForm(ModelForm):
    class Meta:
        model = Unidad
        fields = '__all__'
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }