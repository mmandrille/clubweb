#Imports django
from django.contrib import admin
#Imports del proyecto
try:
    from personal.admin import EmpleadoInline
except ImportError:
    EmpleadoInline = None
#Import de la app
from .form import UnidadForm
from .models import Unidad

#Definimos Admins
class UnidadAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    if EmpleadoInline:
        inlines = [EmpleadoInline]
    autocomplete_fields = ("padre",)
    form = UnidadForm

# Register your models here.
admin.site.register(Unidad, UnidadAdmin)

