#Imports de Python
import json
#Imports de Django
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
#Imports del proyecto
from clubweb.constantes import MAX_CHILDS
#Import Personales
from .models import Unidad
from .functions import calcular_ancho

# Create your views here.
def home(request):
    max_child = MAX_CHILDS
    origen = Unidad.objects.filter(primario=True).order_by('id').first()
    if not origen:
        origen = Unidad.objects.first()
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organigrama_limit.html', {'origen': origen, 'max_child': max_child, 'ancho': ancho}) 

def full_arbol(request):
    max_child = 50
    origen = Unidad.objects.filter(primario=True).order_by('id').first()
    if not origen:
        origen = Unidad.objects.first()
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organigrama_full.html', {'origen': origen, 'ancho': ancho})

def organismo(request, origen_id):
    max_child = MAX_CHILDS
    origen = Unidad.objects.get(id=origen_id, activo=True)
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organigrama_limit.html', {'origen': origen, 'max_child': max_child, 'ancho': ancho}) 

def org_limit(request, padre_id, max_child):
    origen = Unidad.objects.get(id=padre_id)
    ancho = calcular_ancho(origen, max_child)
    return render(request, 'organigrama_limit.html', {'origen': origen, 'max_child': max_child, 'ancho': ancho})

def listado(request):
    origen = Unidad.objects.filter(primario=True).order_by('id').first()
    if not origen:
        origen = Unidad.objects.first()
    return render(request, 'lista/lista.html', {'origen': origen})

def simple_list(request):
    origen = Unidad.objects.filter(primario=True).order_by('id').first()
    if not origen:
        origen = Unidad.objects.first()
    return render(request, 'lista/simple_list.html', {'origen': origen})

def crear_sub_org(request, id_padre):
    new_org = Unidad()
    new_org.padre = Unidad.objects.get(pk=id_padre)
    new_org.nombre = 'SubUnidad'
    new_org.save()
    return HttpResponseRedirect('/admin/organigrama/unidad/'+ str(new_org.id) + '/change/')