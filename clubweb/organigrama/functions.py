def calcular_ancho(origen, maximo, dict_ancho=None, lvl=0):
    if not dict_ancho:
        dict_ancho = {0:1, }

    if lvl < maximo and origen.hijos.all():
        if not (lvl+1) in dict_ancho:
            #Instanciamos el siguiente lvl
            dict_ancho[lvl+1] = 0
        for hijo in origen.hijos.all():
            dict_ancho[lvl+1] += calcular_ancho(hijo, maximo, dict_ancho, lvl+1)

    if lvl != 0:#Si no es 0 > La raiz
        return 1
    else:
        #Si llegamos al final, vemos cual es el nivel mas ancho
        ancho = 0
        for valor in dict_ancho.values():
            if ancho < valor:
                ancho = valor
        #Lo multiplicamos por la cantidad de pixeles que queremos
        return ancho * 1000