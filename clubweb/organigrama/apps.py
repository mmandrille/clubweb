from django.apps import AppConfig


class OrganigramaConfig(AppConfig):
    name = 'organigrama'
    def ready(self):
        #Agregamos al menu
        from core.apps import CoreConfig
        CoreConfig.ADMIN_MENU += [('Organigrama', self.name)]
