from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'organigrama'
urlpatterns = [
    #Personales
    path('', views.home, name='home'),
    path('full', views.full_arbol, name='full_arbol'),
    path('<int:origen_id>', views.organismo, name='organismo'),
    path('origen/<int:padre_id>/max/<int:max_child>/', views.org_limit, name='org_limit'),

    #Generar Listado
    path('lista', views.listado, name='listado'),
    path('lista_edit', views.listado, name='listado'),
    path('simple_list', views.simple_list, name='simple_list'),

    #Edicion
    path('crear_hijo/<int:id_padre>/', views.crear_sub_org, name='crear_sub_org'),
]