#Import Python Standard
import json
#Imports de Django
from django.apps import apps
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
#Imports de la app
from .models import Provincia, Departamento, Localidad
from .forms import UploadCsv

# Create your views here.
@staff_member_required
def upload_localidades(request):
    if request.method == "GET":
        titulo = "Carga Masiva Geografica"
        form = UploadCsv()
        return render(request, "upload_csv.html", {'titulo': titulo, 'form': form, })
    else:
        form = UploadCsv(request.POST, request.FILES)
        if form.is_valid():
            file_data = form.cleaned_data['csvfile'].read().decode("utf-8")
            lines = file_data.split("\n")
            cant = 0
            #Limpiamos la base de datos:
            Provincia.objects.all().delete()
            p = Provincia(nombre=form.cleaned_data['provincia'])
            p.save()
            #GEneramos todos los elementos nuevos
            for linea in lines:
                cant += 1
                linea=linea.split(',')
                if linea[0]:
                    departamento = Departamento.objects.get_or_create(
                        provincia=p,
                        nombre= linea[1])[0]
                    localidad = Localidad.objects.get_or_create(
                        departamento= departamento,
                        nombre= linea[2],
                        codigo_postal=linea[0])[0]
            return render(request, 'upload_csv.html', {'count': len(lines), })
        else:
            message = form.errors
            return render(request, "upload_csv.html", {'message': message, })

@staff_member_required
def ws_georef(request, nombre_modelo=None):
    if nombre_modelo:
        if apps.all_models['georef'][nombre_modelo.lower()]:
            modelo = apps.all_models['georef'][nombre_modelo.lower()]
            if hasattr(modelo, 'as_dict'):
                datos = modelo.objects.all()
                datos = [d.as_dict() for d in datos]
                return HttpResponse(json.dumps({nombre_modelo+'s': datos, "cant_registros": len(datos),}), content_type='application/json')
    nombres = []
    for nombre, modelo in apps.all_models['georef'].items():
        if hasattr(modelo, 'as_dict'):
            nombres.append(modelo.__name__ )
    return render(request, 'ws_georef.html', {"modelos": nombres,})