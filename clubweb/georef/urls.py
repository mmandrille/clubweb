from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'georef'
urlpatterns = [
    #Web Services
    path('upload', views.upload_localidades, name='upload_localidades'),
]