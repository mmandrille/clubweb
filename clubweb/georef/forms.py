from django import forms

#Definimos nuestros formularios
class UploadCsv(forms.Form):
    provincia = forms.CharField(max_length=100, initial="Santa Fe")
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))