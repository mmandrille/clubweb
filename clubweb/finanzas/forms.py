#Imports standard de python
from datetime import datetime
#Import Standard de django
from django import forms

#imports del proyecto

#Definimos forms
class PeriodoForm(forms.Form):
    month = forms.ChoiceField(label='Mes', choices=[(m,m) for m in range(1,13)], initial=datetime.now().month)
    year = forms.ChoiceField(label='Año', choices=[(y,y) for y in range(2019, datetime.today().year+1)], initial=datetime.now().year)

class RangoFechasForm(forms.Form):
    begda = forms.DateField(label="Inicio del Informe", widget=forms.SelectDateWidget(years = reversed(range(2000, datetime.today().year+1))))
    endda = forms.DateField(label="Fin del Informe", widget=forms.SelectDateWidget(years = reversed(range(2000, datetime.today().year+1))))