#Imports Django
from django.conf.urls import url
from django.urls import path
#Import personales
from . import views

app_name = 'finanzas'
urlpatterns = [
    #Basicas:
    path('', views.finanzas_menu, name='finanzas_menu'),
    #reportes
    path('recolectar', views.recolectar, name='recolectar'),
    path('recoleccion/<int:month>/<int:year>', views.ver_recoleccion, name='ver_recoleccion'),
    path('delete/<int:year>/<int:month>', views.delete, name='delete'),
    path('reporte', views.reporte, name='reporte'),
    url(r'detalle/(?P<tipo>\w{2})/(?P<begda>\d{4}-\d{2}-\d{2})/(?P<endda>\d{4}-\d{2}-\d{2})/$', views.ver_detalles, name='ver_detalles'),
    path('cierre/<int:id_cierre>', views.ver_cierre, name='ver_cierre'),
    #Guardar el periodo
    url(r'cerrar/(?P<begda>\d{4}-\d{2}-\d{2})/(?P<endda>\d{4}-\d{2}-\d{2})/$', views.cerrar_periodo, name='cerrar_periodo'),
    #Graficos
    path('historico', views.historico, name='historico'),
    url(r'historico/(?P<begda>\d{4}-\d{2}-\d{2})/(?P<endda>\d{4}-\d{2}-\d{2})/$', views.historico, name='historico'),
    #Managment
    path('download', views.download, name='download'),
]