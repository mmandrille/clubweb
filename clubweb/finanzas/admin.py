
#Imports Django
from django.db import models
from django.contrib import admin
from django.forms import TextInput
from django.contrib.admin import DateFieldListFilter
#Imports de la app
from .models import Asiento, Cierre, Detalle

#Definimos los Inlines
class DetalleInline(admin.TabularInline):
    model = Detalle
    fk_name = 'cierre'
    extra = 0

# Definimos los Model Admins
class AsientoAdmin(admin.ModelAdmin):
    model = Asiento
    search_fields = ['descripcion']
    list_filter = ['origen', 'tipo', ('fecha', DateFieldListFilter),]
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
    }

class CierreAdmin(admin.ModelAdmin):
    model = Cierre
    list_filter = [('fecha', DateFieldListFilter)]
    inlines = [DetalleInline]
    readonly_fields = ['mes_cierre', 'ano_cierre', 'fecha']

#Registramos nuestros Modelos
admin.site.register(Asiento, AsientoAdmin)
admin.site.register(Cierre, CierreAdmin)