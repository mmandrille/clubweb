#Imports de Python
#Imports de Django
from django import template

register = template.Library()
#Definimos nuestros tags
@register.simple_tag
def agrupar_origenes(asientos):
    listado = {}
    for asiento in asientos:
        if asiento.origen in listado:
            listado[asiento.origen] += 1
        else:
            listado[asiento.origen] = 1
    return listado