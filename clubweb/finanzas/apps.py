#Imports de Django
from django.apps import AppConfig


class FinanzasConfig(AppConfig):
    name = 'finanzas'
    APPS_FINANCIERAS = []
    def ready(self):
        #Agregamos al menu
        from core.apps import CoreConfig
        CoreConfig.ADMIN_MENU += [('Finanzas', self.name)]
