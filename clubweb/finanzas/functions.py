#Imports Python
from importlib import import_module
#Imports Django
#imports del proyecto
from clubweb.settings import INSTALLED_APPS
#Imports de la app
from .models import Asiento, Cierre

#Definimos funciones Genericas
def hay_recoleccion(begda, endda):
    asientos = Asiento.objects.filter(fecha__range=(begda, endda))
    asientos = asientos.exclude(origen='Finanzas')
    asientos = asientos.exclude(origen='Cierre')
    if asientos:
        return True
    else:
        return False

def cierre_realizado(year, month):
    cierres = Cierre.objects.filter(ano_cierre=year, mes_cierre=month)
    if cierres:
        return True
    else:
        return False

def obtener_recolecciones():
    periodos = []
    asientos = Asiento.objects.all()
    asientos = asientos.exclude(origen='Finanzas')
    asientos = asientos.exclude(origen='Cierre')
    asientos = asientos.values_list('fecha').distinct()
    for asiento in asientos:
        periodo = str(asiento[0].month) + '/' +  str(asiento[0].year)
        if periodo not in periodos:
            periodos.append(periodo)
    return periodos

def obtener_cierres():
    return Cierre.objects.all().order_by('-fecha')

def totalizar_asientos(asientos):
    #Empezamos a generar los informes
    acumulados = {
                'Saldo Inicial': ['SI',0,],
                'Ingresos': ['IN',0,],
                'Egresos': ['EG',0,],
                'Deudas por Cobrar': ['DC',0,],
                'Cuentas por pagar': ['DP',0,],
                'Resultado': ['RE',0,],
    }
    #Recorremos los asientos y los agrupamos por categoria
    for asiento in asientos:
        acumulados[asiento.get_tipo_display()][1] = acumulados[asiento.get_tipo_display()][1] + asiento.monto
    #Calculamos el resultado
    acumulados['Resultado'][1] = acumulados['Saldo Inicial'][1] + acumulados['Ingresos'][1] - acumulados['Egresos'][1] + acumulados['Deudas por Cobrar'][1] - acumulados['Cuentas por pagar'][1]
    return acumulados