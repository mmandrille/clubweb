#imports python
import datetime
#Imports django
from django.db import models
#Imports de la app
from .choices import TIPO_ASIENTO

# Create your models here.
#Modulo de PRESUPUESTO FINANCIERO
class Asiento(models.Model):
    origen = models.CharField(verbose_name="Modulo de Origen", max_length=20, default="Finanzas")
    tipo = models.CharField('Tipo Asiento', max_length=2, choices=TIPO_ASIENTO, default='SI')
    fecha = models.DateField(verbose_name="Fecha")
    monto = models.DecimalField(verbose_name="Importe", max_digits=8, decimal_places=2)
    descripcion = models.CharField('Descripcion', max_length=100)
    def __str__(self):
        return self.get_tipo_display() + ' | ' + str(self.fecha) + ' $' + str(self.monto) + ' - ' + self.descripcion
    def as_dict(self):
        return {
            'id': self.id,
            'origen':  self.origen,
            'tipo_id': self.tipo,
            'tipo': str(self.get_tipo_display()),
            'fecha': str(self.fecha),
            'monto': str(self.monto),
            'descripcion': self.descripcion,
        }

class Cierre(models.Model):
    mes_cierre = models.IntegerField('Mes') 
    ano_cierre = models.IntegerField('Año')
    fecha = models.DateField(verbose_name="Fecha")
    def __str__(self):
        return 'Cierre ' + str(self.mes_cierre) + '/' + str(self.ano_cierre)
    def as_dict(self):
        return {
            'id': self.id,
            'mes_cierre':  self.mes_cierre,
            'ano_cierre': self.ano_cierre,
            'fecha': str(self.fecha),
        }

class Detalle(models.Model):
    cierre = models.ForeignKey(Cierre, on_delete=models.CASCADE, related_name="detalles")
    tipo = models.CharField('Tipo Asiento', max_length=2, choices=TIPO_ASIENTO, default='SI')
    monto = models.DecimalField(verbose_name="Importe", max_digits=12, decimal_places=2)
    def __str__(self):
        return 'Detalle de ' + str(self.cierre) + ' ,tipo:' + str(self.tipo) +' $' + str(self.monto)
    def as_dict(self):
        return {
            'id': self.id,
            'cierre_id':  self.cierre.id,
            'cierre': str(self.cierre),
            'tipo_id': self.tipo,
            'tipo': str(self.get_tipo_display()),
            'monto': str(self.monto),
        }