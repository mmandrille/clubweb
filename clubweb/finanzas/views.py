#Imports Python
import csv
import calendar
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
#Imports Django
from django.shortcuts import render
from django.http import HttpResponse
#decoradores
from django.contrib.admin.views.decorators import staff_member_required
#Imports del proyecto

#Imports de la app
from .apps import FinanzasConfig
from .models import Asiento, Cierre, Detalle
from .forms import PeriodoForm, RangoFechasForm
from .functions import obtener_cierres, obtener_recolecciones
from .functions import totalizar_asientos
from .functions import hay_recoleccion, cierre_realizado

# Create your views here.
@staff_member_required
def finanzas_menu(request):
    reserva = Asiento.objects.filter(tipo='SI')#('SI', 'Saldo Inicial')
    reserva = reserva.order_by('fecha').last()
    return render(request, "menu_finanzas.html", {
        'reserva': reserva, 
        'recolecciones': obtener_recolecciones(),
        'cierres': obtener_cierres(),})

@staff_member_required
def recolectar(request, year=None, month=None):
    if request.method == "GET" and not year:
        form = PeriodoForm()
        return render(request, 'form_finanzas.html', {
            'recolecciones': obtener_recolecciones(),
            'cierres': obtener_cierres(),
            'form': form, 
            'titulo': 'Recoleccion de Asientos Contables', 
            'boton': 'Recolectar', })
    else:
        if year is None:
            form = PeriodoForm(request.POST)
            if form.is_valid():
                #Generamos fecha de inicio y fin de la busqueda
                year = int(form.cleaned_data['year'])
                month = int(form.cleaned_data['month'])
            else:
                return render(request, 'form_finanzas.html', {
                    'recolecciones': obtener_recolecciones(),
                    'cierres': obtener_cierres(),
                    'form': form, 'message': form.errors,
                    'titulo': 'Recoleccion de Asientos Contables', 'boton': 'Recolectar', })

        #Generamos fecha de inicio y fin
        begda = date(year, month, 1)
        endda = date(year, month, calendar.monthrange(year,month)[1])
        #Chequeamos que no se haya realizado cierre!
        if cierre_realizado(year, month):
            return render(request, 'form_finanzas.html', {
                'form': form,
                'recolecciones': obtener_recolecciones(),
                'cierres': obtener_cierres(),
                'message': 'Imposible recolectar Asientos si ya fue realizado el cierre del periodo.',
                'titulo': 'Recoleccion de Asientos Contables', 'boton': 'Recolectar', })
        
        #Chequeamos si ya existen Asientos de apps:
        if hay_recoleccion(begda, endda):
            return render(request, "eliminar_asientos.html", {'count': hay_recoleccion(begda, endda), 'periodo': str(year)+'/'+str(month)})

        #Recolectamos asientos de cada app:
        origenes = []
        movimientos = []
        for app in FinanzasConfig.APPS_FINANCIERAS:
            origenes+= [app[0].capitalize()]
            movimientos+= app[1](begda, endda)

        #Guardamos los elementos recolectados
        Asiento.objects.bulk_create(movimientos)
        #Lanzamos reporte
        return render(request, "reporte_recoleccion.html", {'origenes': origenes, 'movimientos': movimientos})

@staff_member_required
def ver_recoleccion(request, month, year):
    begda = date(year, month, 1)
    endda = date(year, month, calendar.monthrange(year,month)[1])
    origenes = [x[0] for x in Asiento.objects.filter(fecha__range=(begda, endda)).values_list('origen').distinct()]
    movimientos = Asiento.objects.filter(fecha__range=(begda, endda))
    return render(request, "reporte_recoleccion.html", {'origenes': origenes, 'movimientos': movimientos})

@staff_member_required
def delete(request, year, month):
    begda = date(year, month, 1)
    endda = date(year, month, calendar.monthrange(year,month)[1])
    asientos = Asiento.objects.filter(fecha__range=(begda, endda))
    asientos = asientos.exclude(origen='Finanzas')
    asientos = asientos.exclude(origen='Cierre')
    asientos.delete()
    return recolectar(request, year, month)

@staff_member_required
def reporte(request, year=None, month=None):
    if request.method == "GET" and not year:
        form = PeriodoForm()
        return render(request, 'form_finanzas.html', {
        'recolecciones': obtener_recolecciones(),
        'cierres': obtener_cierres(),
        'form': form, 
        'titulo': 'Reporte Mensual', 
        'boton': 'Recolectar', })
    else:
        if year is None:
            form = PeriodoForm(request.POST)
            if form.is_valid():
                #Generamos fecha de inicio y fin de la busqueda
                year = int(form.cleaned_data['year'])
                month = int(form.cleaned_data['month'])
            else:
                message = form.errors
                return render(request, 'form_finanzas.html', {
                    'recolecciones': obtener_recolecciones(),
                    'cierres': obtener_cierres(),
                    'form': form,
                    'message': message,
                    'titulo': 'Reporte Mensual', 
                    'boton': 'Recolectar', })
        #Generamos fechas de inicio y fin para el reporte
        begda = date(year, month, 1)
        endda = date(year, month, calendar.monthrange(year,month)[1])
        #Chequeamos que no se haya realizado cierre!
        if cierre_realizado(year, month):
            return render(request, 'form_finanzas.html', {
                'form': form, 
                'recolecciones': obtener_recolecciones(),
                'cierres': obtener_cierres(),
                'message': 'Imposible Generar Cierre si ya fue realizado. Consultelo en el menu de la derecha.',
                'titulo': 'Reporte Mensual', 'boton': 'Generar', })
        #Generamos fechas necesarias
        ano_anterior = (begda - timedelta(days=1)).year
        mes_anterior = (begda - timedelta(days=1)).month
        #inicio_mes_anterior = date(ano_anterior, mes_anterior, 1)
        #fin_mes_anterior = begda - timedelta(days=1)
        #Chequeamos que se haya realizado el cierre del periodo previo:
        if not cierre_realizado(ano_anterior, mes_anterior):
            return render(request, 'form_finanzas.html', {
                'form': form,
                'recolecciones': obtener_recolecciones(),
                'cierres': obtener_cierres(),
                'message': "Falta Generar el Cierre para el periodo " + str(mes_anterior) + "/" + str(ano_anterior),
                'titulo': 'Reporte Mensual', 
                'boton': 'Generar', })
        #Chequeamos que se haya realizado la recoleccion de asientos previos:
        if not hay_recoleccion(begda, endda):
            return render(request, 'form_finanzas.html', {
                'form': form,
                'recolecciones': obtener_recolecciones(),
                'cierres': obtener_cierres(),
                'message': "Falta Recolectar Asientos para el periodo " + str(begda.month) + "/" + str(begda.year),
                'titulo': 'Reporte Mensual', 
                'boton': 'Generar', })
        #Buscamos todos los asientos contables:
        asientos = Asiento.objects.filter(fecha__range=(begda, endda))
        acumulados = totalizar_asientos(asientos)
        return render(request, 'reporte_financiero.html', {'begda': begda, 'endda': endda , 'acumulados': acumulados,})

@staff_member_required
def ver_detalles(request, tipo, begda, endda):
    asientos = Asiento.objects.filter(tipo=tipo, fecha__range=(begda, endda))
    return render(request, 'detalle_asientos.html', {'tipo': tipo, 'begda': begda, 'endda': endda , 'asientos': asientos,})

@staff_member_required
def cerrar_periodo(request, begda, endda):
    #Obtenemos mes siguiente
    fecha_siguiente = datetime.strptime(endda, "%Y-%m-%d").date() + timedelta(days=1)
    #Obtenemos todos los asientos del periodo:
    asientos = Asiento.objects.filter(fecha__range=(begda, endda))
    #Creamos objeto de Cierre
    cierre = Cierre(mes_cierre=begda[5:7], ano_cierre=begda[0:4], fecha=datetime.today().date())
    cierre.save()
    #procesamos los asientos:
    acumulados = totalizar_asientos(asientos)
    for acumulado in acumulados.values():
        detalle = Detalle()
        detalle.cierre = cierre
        detalle.tipo = acumulado[0]
        detalle.monto = acumulado[1]
        detalle.save()
    # Creamos Saldo Inicial
    asiento = Asiento()
    asiento.origen = 'Cierre'
    asiento.tipo = 'SI'
    asiento.fecha = fecha_siguiente
    asiento.monto = acumulados['Saldo Inicial'][1] + acumulados['Ingresos'][1] - acumulados['Egresos'][1]
    asiento.descripcion = 'Generado Automaticamente de Periodo' + begda[0:7]
    asiento.save()
    # Creamos deudas a cobrar GENERICA
    asiento = Asiento()
    asiento.origen = 'Cierre'
    asiento.tipo = 'DC'
    asiento.fecha = fecha_siguiente
    asiento.monto = acumulados['Deudas por Cobrar'][1]
    asiento.descripcion = 'Generado Automaticamente de Periodo' + begda[0:7]
    asiento.save()
    # Creamos deudas a pagar GENERICA
    asiento = Asiento()
    asiento.origen = 'Cierre'
    asiento.tipo = 'DP'
    asiento.fecha = fecha_siguiente
    asiento.monto = acumulados['Cuentas por pagar'][1]
    asiento.descripcion = 'Generado Automaticamente de Periodo' + begda[0:7]
    asiento.save()
    return render(request, 
                    'cierre_periodo.html', 
                    {'cierre': cierre, 'count':len(asientos), 
                    'asientos': Asiento.objects.filter(fecha=fecha_siguiente, origen='Cierre'), 
                    'begda': begda, 'endda': endda , })

def ver_cierre(request, id_cierre):
    cierre = Cierre.objects.get(id=id_cierre)
    begda = date(cierre.ano_cierre, cierre.mes_cierre, 1)
    endda = date(begda.year, begda.month, calendar.monthrange(begda.year,begda.month)[1])
    return render(request, 
                'cierre_periodo.html', 
                {'cierre': cierre,
                'asientos': Asiento.objects.filter(origen='Cierre', fecha=endda + timedelta(days=1)), 
                'begda': begda, 'endda': endda , })

def historico(request, begda=None, endda=None):
    cierres = Cierre.objects.all()
    if begda and endda:
        cierres = cierres.filter(fecha__range=(begda, endda))
    return render(request, 'graficos/historico_cierres.html', {'cierres': cierres, })

#@staff_member_required
def download(request):
    if request.method == "GET":
        form = RangoFechasForm()
        return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Descarga de Asientos', 'boton': 'Descargar'})
    else:
        form = RangoFechasForm(request.POST)
        if form.is_valid():
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="asientos.csv"'
            writer = csv.writer(response)
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            asientos = Asiento.objects.filter(fecha__range=(begda, endda))
            writer.writerow(['Periodo:', begda, endda])
            writer.writerow(['Modulo', 'Tipo Asiento', 'Fecha', 'Importe', 'Descripcion'])
            for asiento in asientos:
                writer.writerow([asiento.origen , asiento.get_tipo_display() , asiento.fecha , asiento.monto , asiento.descripcion])
            return response
        else:
            message = form.errors
            return render(request, 'extras/generic_form.html', {'form': form, 'message': message, 'titulo': 'Descarga de Asientos', 'boton': 'Descargar'})