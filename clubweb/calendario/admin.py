#Imports standard de Django
from django.contrib import admin
#imports de la App
from .models import Tipo_Evento, Evento

#Models Ocultos
def register_hidden_models(*model_names):
    for m in model_names:
        ma = type(
            str(m)+'Admin',
            (admin.ModelAdmin,),
            {
                'get_model_perms': lambda self, request: {}
            })
        admin.site.register(m, ma)

# Register your models here.
class EventoAdmin(admin.ModelAdmin):
    list_filter = ['importante']

#Los mandamos al admin
register_hidden_models(Tipo_Evento)
admin.site.register(Evento, EventoAdmin)
