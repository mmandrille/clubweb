#Imports de python
import datetime
#Import de Django
from django.db import models
from django.contrib.auth.models import User
#Imports Extras
from tinymce.models import HTMLField


#Create your models here.
class Tipo_Evento(models.Model):
    orden = models.SmallIntegerField(unique=True)
    nombre = models.CharField(verbose_name="Tipo de Socio", max_length=50)
    def __str__(self):
        return self.nombre

class Evento(models.Model):
    nombre = models.CharField('Titulo', max_length=200)
    portada = models.FileField(upload_to='eventos/portadas/', default='//static/img/noimage.png')
    tipo = models.ForeignKey(Tipo_Evento, on_delete=models.CASCADE, related_name="socios", blank=True, null=True)
    descripcion = HTMLField()
    fecha_inicio = models.DateTimeField('Fecha del Evento', default=datetime.datetime.now)
    fecha_fin = models.DateTimeField('Fecha de fin del Evento', default=datetime.datetime.now)
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    importante = models.BooleanField(default=False)
    def __str__(self):
        return self.nombre + ' ' + str(self.fecha_inicio)
    def as_dict(self):
        return {
            'id': self.id,
            'nombre':  self.nombre,
            'portada': str(self.portada),
            'tipo': str(self.tipo),
            'descripcion': self.descripcion,
            'fecha_inicio': str(self.fecha_inicio),
            'fecha_fin': str(self.fecha_fin),
            'autor': str(self.autor),
            'importante': self.importante,
        }