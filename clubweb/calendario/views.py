#Imports de Django
from django.shortcuts import render
from django.utils import timezone
#Import Personales
from .models import Evento

# Create your views here.
def calendario(request):
    eventos = Evento.objects.filter(fecha_fin__gt=timezone.now(), importante=True).order_by('-fecha_inicio')
    return render(request, 'calendario.html', {'eventos': eventos, 'hoy' : timezone.now().strftime('%d/%m/%Y')})

def ver_evento(request, evento_id):
    eventos = Evento.objects.filter(fecha_fin__gt=timezone.now()).order_by('-fecha_inicio')[:3]
    evento = Evento.objects.get(pk=evento_id)
    return render(request, 'evento.html', {'eventos': eventos, 'evento': evento, })