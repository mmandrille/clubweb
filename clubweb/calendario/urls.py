from django.conf.urls import url
from django.urls import path
#Import de modulos personales
from . import views

app_name = 'calendario'
urlpatterns = [
    path('', views.calendario, name='calendario'),
    path('<int:evento_id>', views.ver_evento, name='evento'),
]