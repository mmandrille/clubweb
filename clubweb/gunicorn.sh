#!/bin/bash
cd /opt/clubweb
source venv/bin/activate
cd /opt/clubweb/clubweb
/opt/clubweb/venv/bin/gunicorn clubweb.wsgi -t 600 -b 127.0.0.1:8099 -w 6 --user=servidor --group=servidor --log-file=/opt/clubweb/gunicorn.log 2>>/opt/clubweb/gunicorn.log
