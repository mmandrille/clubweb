#Imports de Python
import csv
import calendar
from datetime import date, timedelta, timedelta
#Imports Django
from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q
#Imports del proyecto

#Imports App
from .models import Dia
from .models import Empleado, TipoHabilidad, Habilidad
from .models import Sector
from .models import Salario, Comportamiento, Concepto, Detalle
from .forms import EmpleadoSearchForm

# Create your views here.
def personal_menu(request):
    cargados = Salario.objects.order_by('fecha_pago').values('fecha_pago').distinct()
    return render(request, "menu_personal.html", {'cargados': cargados, })

def resumen_empleado(request, id_empleado=None):
    if id_empleado:
        try:
            empleado = Empleado.objects.get(pk=id_empleado)
            return render(request, 'empleado_resumen.html', {'empleado': empleado,})
        except Empleado.DoesNotExist:
            return render(request, 'extras/error.html', {})
    else:
        if request.method == "GET":
            form = EmpleadoSearchForm()
            return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Resumen del Empleado', 'boton': 'Buscar'})
        else:
            form = EmpleadoSearchForm(request.POST)
            if form.is_valid():
                try:
                    if form.cleaned_data['num_doc']:
                        empleado = Empleado.objects.get(num_doc=form.cleaned_data['num_doc'])
                    elif form.cleaned_data['num_legajo']:
                        empleado = Empleado.objects.get(num_legajo=form.cleaned_data['num_legajo'])
                    return resumen_empleado(request, empleado.id)  
                except Empleado.DoesNotExist:
                    return render(request, 'extras/error.html', {})
            else:
                return render(request, 'extras/error.html', {})

def buscador(request):
    if request.method == 'POST':
        if 'buscar' in request.POST:
            buscar = request.POST['buscar']
            empleados = Empleado.objects.filter(Q(nombres__icontains=buscar)
                    | Q(apellidos__icontains=buscar)
            )
        return render(request, 'lista_empleados.html', {'empleados': empleados, })
    else:
        return resumen_empleado(request)

def buscar_por_habilidad(request):
    if request.method == "GET":
        tiposhabilidades = TipoHabilidad.objects.all()
        return render(request, 'buscar_por_habilidad.html', {'tiposhabilidades': tiposhabilidades, })
    else:
        #Obtenemos las habilidades seleccionadas
        habilidades = [int(id) for id in request.POST.getlist('habilidad')]
        habilidades = Habilidad.objects.filter(id__in=habilidades)
        #Preparamos la lista a entregar
        empleados = []
        for empleado in Empleado.objects.all().exclude(num_legajo=None):
            empleado.puntaje = 0
            for hab in empleado.habilidades.all():
                if hab.habilidad in habilidades:
                    empleado.puntaje+= hab.nivel
            if empleado.puntaje:
                empleados.append(empleado)
                empleados.sort(key=lambda x: x.puntaje, reverse=True)
        return render(request, 'empleados_por_habilidad.html', {'habilidades_buscadas': habilidades, 'empleados': empleados,})

def ver_turnos(request, id_sector=None):
    lista_sectores = Sector.objects.all()
    if id_sector:
        sectores = Sector.objects.filter(id=id_sector)
    else:
        sectores = lista_sectores
    #Fechamos los dias:
    hoy = type('Generic', (), {'fecha':date.today(), 
                            'numero': date.today().weekday()})
    dias = []
    for dia in Dia.objects.all():
        dia.fecha = hoy.fecha + timedelta(days=(dia.numero - hoy.numero))
        dias.append(dia)#Aca tenemos que asignar la fecha del dia en la semana
    return render(request, 'turnos.html', {'sectores': sectores,
                                            'lista_sectores': lista_sectores,
                                            'horas': range(0,24), 'dias': dias,
                                            'id_sector': id_sector, })

def ver_postulantes(request):
    empleados = Empleado.objects.filter(num_legajo=None)
    return render(request, 'postulantes.html', {'empleados': empleados, })

def ver_salarios(request, fecha):
    salarios = Salario.objects.filter(fecha_pago=fecha)
    return render(request, 'listado_salarios.html', {'salarios': salarios, })

def upload_salarios(request):
    if request.method == 'POST' and 'csvfile' in request.FILES:
        csv_file = request.FILES['csvfile']
        file_data = csv_file.read().decode("utf-8")
        lines = file_data.split("\n")
        salarios = []
        for line in lines:
            line = line.replace('\r', '').split(',')
            try:
                empleado = Empleado.objects.get(num_legajo=line[0])
                salario = Salario()
                salario.empleado = empleado
                salario.begda = line[4]
                salario.endda = line[5]
                salario.importe = line[6]
                salario.fecha_pago = line[7]
                if line[8] == "True":
                    salario.pagado = True
                    salario.pagado_fecha = line[9]
                salarios.append(salario)
            except Empleado.DoesNotExist:
                print('No existe el empleado')
        Salario.objects.bulk_create(salarios)
        return render(request, 'listado_salarios.html', {'salarios': salarios, })
    else:
        cargados = Salario.objects.order_by('fecha_pago').values('fecha_pago').distinct()
        return render(request, 'upload_salarios.html', {'cargados': cargados, })

def excel_ejemplo_simple(request):
    response = HttpResponse(content_type='text/csv')
    fecha = date.today()
    begda = date(fecha.year, fecha.month, 1)
    endda = date(begda.year, begda.month, calendar.monthrange(begda.year,begda.month)[1])
    next_month = (fecha.replace(day=1) + timedelta(days=32)).replace(day=1)
    response['Content-Disposition'] = 'attachment; filename="salario'+str(fecha)+'.csv"'
    writer = csv.writer(response)
    writer.writerow(['Archivo Simple'])
    writer.writerow(['Num Legajo', 'Num Doc', 'Apellidos', 'Nombre', 'Inicio Periodo', 'Fin Periodo', 'Importe', 'Fecha de Cobro', 'Pagado (False/True)',  'Fecha de Pago'])
    for empleado in Empleado.objects.exclude(num_legajo=None):
        writer.writerow([empleado.num_legajo, empleado.num_doc, 
            empleado.apellidos, empleado.nombres, 
            begda, endda, 0, 
            date(next_month.year, next_month.month, 10), 
            False, date(9999, 12, 31)])
    return response


def excel_ejemplo_avanzado(request):


    response = HttpResponse(content_type='text/csv')
    fecha = date.today()
    begda = date(fecha.year, fecha.month, 1)
    endda = date(begda.year, begda.month, calendar.monthrange(begda.year,begda.month)[1])
    next_month = (fecha.replace(day=1) + timedelta(days=32)).replace(day=1)
    response['Content-Disposition'] = 'attachment; filename="salario'+str(fecha)+'.csv"'
    writer = csv.writer(response)
    writer.writerow(['Archivo Simple'])
    writer.writerow(['Num Legajo', 'Num Doc', 'Apellidos', 'Nombre', 'Inicio Periodo', 'Fin Periodo', 'Importe', 'Fecha de Cobro', 'Pagado (False/True)',  'Fecha de Pago'])
    for empleado in Empleado.objects.exclude(num_legajo=None):
        writer.writerow([empleado.num_legajo, empleado.num_doc, 
            empleado.apellidos, empleado.nombres, 
            begda, endda, 0, 
            date(next_month.year, next_month.month, 10), 
            False, date(9999, 12, 31)])
    return response