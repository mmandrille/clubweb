#Imports de python

#Imports django

#Import de proyecto
from finanzas.models import Asiento
#Imports de la app
from .models import Salario

#Definimos nuestras funciones informativas
def ingresos(begda, endda):
    return []

def egresos(begda, endda):
    movimentos = []
    salarios = Salario.objects.filter(pagado_fecha__range=(begda, endda), pagado=True)
    for salario in salarios:
        mov = Asiento()
        mov.tipo = 'EG'
        mov.origen = "Personal"
        mov.fecha = salario.pagado_fecha
        mov.monto = salario.importe
        mov.descripcion = 'Salario de ' + salario.empleado.apellidos +', '+salario.empleado.nombres+'. Fecha:'+str(salario.fecha_pago)
        movimentos.append(mov)
        if salario.fecha_pago < begda:#Si estoy pagando un salario atrasado
            #Se genero deuda y hay que cancelarla
            mov = Asiento()
            mov.tipo = 'DC'
            mov.origen = "Personal"
            mov.fecha = salario.pagado_fecha
            mov.monto = - salario.importe
            mov.descripcion = 'Cancelamos Adeudado por Salario de ' + salario.empleado.apellidos +', '+salario.empleado.nombres+'. Fecha:'+str(salario.fecha_pago)
            movimentos.append(mov)
    return movimentos

def por_cobrar(begda, endda):
    return []

def por_pagar(begda, endda):
    movimentos = []
    salarios = Salario.objects.filter(fecha_pago__range=(begda, endda), pagado=False)
    for salario in salarios:
        mov = Asiento()
        mov.tipo = 'DP'
        mov.origen = "Personal"
        mov.fecha = salario.fecha_pago
        mov.monto = salario.importe
        mov.descripcion = 'Adeudado por Salario de ' + salario.empleado.apellidos +', '+salario.empleado.nombres+'. Fecha:'+str(salario.fecha_pago)
        movimentos.append(mov)
    return movimentos

def recoleccion(begda, endda):
    movimientos = ingresos(begda, endda)
    movimientos+= egresos(begda, endda)
    movimientos+= por_cobrar(begda, endda)
    movimientos+= por_pagar(begda, endda)
    return movimientos