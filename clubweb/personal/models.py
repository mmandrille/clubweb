#Imports Python
from datetime import datetime, date
#Imports Django
from django.db import models
from django.contrib.auth.models import User
#Imports Extras

#Imports del proyecto
from organigrama.models import Unidad
#Imports de la app
from .choices import ESTADO_CIVIL, NIVEL_EDUCATIVO, NIVEL_HABILIDAD

# Create your models here.

#Definimos choices incrementables
class Dia(models.Model):
    numero = models.IntegerField()
    nombre = models.CharField('Nombre', max_length=20)
    class Meta:
        ordering = ['numero']
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
        }

class TipoCargo(models.Model):
    nombre = models.CharField('Cargo', max_length=200)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
        }

class TipoDocumento(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
        }

class TipoHabilidad(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
        }

class Sector(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
        }

class Habilidad(models.Model):
    tipo = models.ForeignKey(TipoHabilidad, on_delete=models.CASCADE, related_name='agrupadas')
    nombre = models.CharField('Nombre', max_length=100)
    class Meta:
        verbose_name_plural = 'Habilidades'
        ordering = ['tipo__nombre', 'nombre']
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'tipo':  str(self.tipo),
            'nombre': self.nombre,
        }

#Definimos modelos
class Empleado(models.Model):
    num_legajo = models.CharField('Legajo', max_length=12, blank=True, null=True)
    num_doc = models.CharField('DNI', max_length=10, blank=True, null=True)
    nombres = models.CharField('Nombres', max_length=100)
    apellidos = models.CharField('Apellidos', max_length=100)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento", null=True, blank=True)
    estado_civil = models.IntegerField(choices=ESTADO_CIVIL, default=2)
    foto = models.FileField('Fotografia', upload_to='personal/foto/', blank=True, null=True)
    domicilio = models.CharField('Domicilio', max_length=200, blank=True, null=True)
    email = models.EmailField('Correo Personal', blank=True, null=True)
    telefono = models.CharField('Telefono', max_length=20, blank=True, null=True)
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="empleados")
    def __str__(self):
        return self.nombres + ' ' + self.apellidos
    def as_dict(self):
        return {
            'id': self.id,
            'num_legajo':  self.num_legajo,
            'num_doc': self.num_doc,
            'nombres': self.nombres,
            'apellidos': self.apellidos,
            'fecha_nacimiento': str(self.fecha_nacimiento),
            'estado_civil': self.get_estado_civil_display(),
            'foto': str(self.foto),
            'domicilio': self.domicilio,
            'email': self.email,
            'telefono': self.telefono,
        }
    def tipo_habilidades(self):
        tipos = []
        for hab in self.habilidades.all():
            if hab.habilidad.tipo not in tipos: 
                tipos.append(hab.habilidad.tipo)
        return tipos
    def contrato_actual(self):
        return self.contratos.filter(endda=date(9999, 12, 31)).first()
    def contratos_previo(self):
        return self.contratos.all().exclude(endda=date(9999, 12, 31)).order_by('-endda')

class Contrato(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='contratos')
    unidad = models.ForeignKey(Unidad, on_delete=models.SET_NULL, related_name='contratos', blank=True, null=True)
    cargo = models.ForeignKey(TipoCargo, on_delete=models.SET_NULL, related_name='contratos', blank=True, null=True)
    funcion_unica = models.BooleanField(default=False)
    subcargo = models.CharField('SubCargo', max_length=20, blank=True, null=True)
    begda = models.DateField('Inicio de Funciones',  default=datetime.now, blank=True, null=True)
    endda = models.DateField('Cese Funciones', default=date(9999, 12, 31))
    def save(self, *args, **kwargs):#para que no haya mas de un empleados en un cargo unico
        if self.funcion_unica:#tener en cuenta otros funcionarios
            Contrato.objects.filter(cargo=self.cargo,subcargo=self.subcargo).exclude(empleado__pk=self.empleado.pk).update(endda=self.begda)
        super(Contrato, self).save(*args, **kwargs)
    class Meta:
        ordering = ['-endda']
    def __str__(self):
        return self.cargo.nombre + ' en ' + self.unidad.nombre
    def as_dict(self):
        return {
            'id': self.id,
            'empleado':  self.empleado.id,
            'unidad': str(self.unidad),
            'cargo': str(self.cargo),
            'funcion_unica': self.funcion_unica,
            'subcargo': self.subcargo,
            'begda': str(self.begda),
            'endda': str(self.endda),
        }

class Escolaridad(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='escolaridad')
    nivel = models.IntegerField(choices=NIVEL_EDUCATIVO, default=3)
    titulo = models.CharField('Titulo Profesional', max_length=100)
    establecimiento = models.CharField('Establecimiento Educativo', max_length=100)
    archivo = models.FileField(verbose_name='Archivo', upload_to='personal/escolaridad/', blank=True, null=True)
    begda = models.DateField('Fecha Inicio',  default=datetime.now, blank=True, null=True)
    endda = models.DateField('Fecha Fin', blank=True, null=True)
    terminado = models.BooleanField(default=False)
    def __str__(self):
        if self.terminado:
            tmp = ': Recibido: ' + str(self.endda.month) + '/' + str(self.endda.year)
        else:
            tmp = ': Sin terminar'
        return self.titulo + ' en ' + self.establecimiento + tmp
    def as_dict(self):
        return {
            'id': self.id,
            'empleado':  self.empleado.id,
            'nivel': self.get_nivel_display(),
            'titulo': self.titulo,
            'establecimiento': self.establecimiento,
            'archivo': str(self.archivo),
            'begda': str(self.begda),
            'endda': str(self.endda),
            'terminado': self.terminado,
        }

class Documento(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='documentos')
    tipo = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE, related_name='asignados')
    archivo = models.FileField(verbose_name='Archivo', upload_to='personal/documentacion/')
    descripcion = models.CharField('Descripcion', max_length=200)
    fecha = models.DateField(verbose_name="Fecha de Recepcion", default=datetime.now)#amo a mi novia
    def as_dict(self):
        return {
            'id': self.id,
            'empleado':  self.empleado.id,
            'tipo': str(self.tipo),
            'archivo': str(self.archivo),
            'descripcion': self.descripcion,
            'fecha': str(self.fecha),
        }

class HabilidadAdquirida(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='habilidades')
    habilidad = models.ForeignKey(Habilidad, on_delete=models.CASCADE, related_name='habilidades')
    nivel = models.IntegerField(choices=NIVEL_HABILIDAD, default=1)
    descripcion = models.CharField('Descripcion', max_length=200, blank=True, null=True)
    def __str__(self):
        return self.habilidad.nombre + ': ' + self.get_nivel_display()
    def as_dict(self):
        return {
            'id': self.id,
            'empleado':  self.empleado.id,
            'habilidad': str(self.habilidad),
            'nivel': self.get_nivel_display(),
            'descripcion': self.descripcion,
        }

#Turnos
class Turno(models.Model):
    sector = models.ForeignKey(Sector, on_delete=models.CASCADE, related_name='turnos')
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='turnos')
    begda = models.DateField('Validez desde',  default=datetime.now)
    endda = models.DateField('Hasta', default='31/12/9999')
    class Meta:
        ordering = ['-endda']
        unique_together = [['empleado', 'endda']]
    def __str__(self):
        return self.empleado.apellidos + ', ' + self.empleado.nombres + ' en ' + self.sector.nombre

class Horario(models.Model):
    turno = models.ForeignKey(Turno, on_delete=models.CASCADE, related_name='horarios')
    dias = models.ManyToManyField(Dia)
    ingreso = models.TimeField('Horario de Ingreso', default='08:00:00')
    salida = models.TimeField('Horario de Salida', default='16:00:00')
    def __str__(self):
        return str(self.turno) + ' horario:' + str(self.ingreso) + ' hasta ' + str(self.salida)

#Sueldos
class Salario(models.Model):
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE, related_name='salarios')
    begda = models.DateField('Desde',  default=datetime.now)
    endda = models.DateField('Hasta',  default=datetime.now)
    fecha_pago = models.DateField('Fecha del Pago',  default=datetime.now)
    importe = models.DecimalField(verbose_name="Importe", max_digits=8, decimal_places=2)
    pagado = models.BooleanField(default=False)
    pagado_fecha = models.DateField('Fecha en la que fue pagado',  default=datetime.now, blank=True, null=True)
    def __str__(self):
        return self.empleado.nombres+' '+self.empleado.apellidos+': $' +str(self.importe)+ ', Desde: '+str(self.begda)+' a '+str(self.endda)
    def calcular(self):
        self.importe = 0
        for detalle in self.detalles.all():
            self.importe+= detalle.importe#deberiamos tener en cuenta el concepto!
        self.save()
    def save(self, *args, **kwargs):
        if self.pagado and not self.pagado_fecha:
            self.pagado_fecha = datetime.now()
        super(Salario, self).save(*args, **kwargs)

class Comportamiento(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    #Aca deberiamos poder storear funciones (Suma, Resta, Multiplica)
    def __str__(self):
        return self.nombre

class Concepto(models.Model):
    contrato = models.ForeignKey(Contrato, on_delete=models.CASCADE, related_name='conceptos')
    comportamiento = models.ForeignKey(Comportamiento, on_delete=models.CASCADE, related_name='conceptos')
    nombre = models.CharField('Nombre', max_length=100)
    begda = models.DateField('Desde',  default=datetime.now)
    endda = models.DateField('Hasta',  default=datetime.now)
    importe = models.DecimalField(verbose_name="Importe", max_digits=8, decimal_places=2, blank=True, null=True)
    def __str__(self):
        return self.nombre

class Detalle(models.Model):
    salario = models.ForeignKey(Salario, on_delete=models.CASCADE, related_name='detalles')
    concepto = models.ForeignKey(Concepto, on_delete=models.CASCADE, related_name='detalles')
    importe = models.DecimalField(verbose_name="Importe", max_digits=8, decimal_places=2, blank=True, null=True)


#Importamos señanesl
from .signals import generar_num_legajo, baja_contrato_anterior