#Imports Django
from django.urls import path
from django.conf.urls import url
#Import personales
from . import views

app_name = 'personal'
urlpatterns = [
    #Basicas:
    path('', views.personal_menu, name='personal_menu'),

    #Menu:
    path('buscar', views.resumen_empleado, name='resumen_empleado'),
    path('buscarhabilidades', views.buscar_por_habilidad, name='buscar_por_habilidad'),
    path('turnos', views.ver_turnos, name='ver_turnos'),
    path('postulantes', views.ver_postulantes, name='ver_postulantes'),
    #Busquedas
    path('buscar/<int:id_empleado>', views.resumen_empleado, name='resumen_empleado'),
    path('turnos/<int:id_sector>', views.ver_turnos, name='ver_turnos'),
    path('buscador', views.buscador, name='buscador'),
    #Salarios
    url(r'salarios/(?P<fecha>\d{4}-\d{2}-\d{2})/$', views.ver_salarios, name='ver_salarios'),
    path('upload_salarios', views.upload_salarios, name='upload_salarios'),
    path('excel_ejemplo_simple', views.excel_ejemplo_simple, name='excel_ejemplo_simple'),
    path('excel_ejemplo_avanzado', views.excel_ejemplo_avanzado, name='excel_ejemplo_avanzado'),
]