#Imports de Python
import datetime
#Imports de Django
from django import template
from django.db.models import Q
#Imports de la pp
from personal.models import Salario
from personal.models import Horario, Turno

register = template.Library()
#Definimos nuestros tags
@register.simple_tag
def turno(sector, dia, hr):
    #Transformamos nuestra hora en un objeto valido para querys
    hora = datetime.time(hr, 0, 0)
    #Obtenemos todos los que sean para la celda de esa hora
    turnos = Turno.objects.filter(
        Q(sector=sector) &
        Q(begda__lte=dia.fecha) & Q(endda__gte=dia.fecha)
    )
    horarios = Horario.objects.filter(
        Q(turno__in=turnos) &
        Q(dias=dia) &
        Q(ingreso__lte=hora) &
        Q(salida__gte=hora)
    )
    return [h.turno.empleado for h in horarios]

@register.simple_tag
def salarios_cargados():
    return Salario.objects.order_by('fecha_pago').values('fecha_pago').distinct()