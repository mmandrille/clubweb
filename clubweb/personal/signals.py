#Imports de python
from datetime import date, timedelta
#Imports de Django
from django.dispatch import receiver
from django.db.models import Max
from django.db.models.signals import post_save

#Imports de la app
from .models import Empleado, Contrato

#Definimos nuestra señales
@receiver(post_save, sender=Contrato)
def generar_num_legajo(instance, **kwargs):
    #Automatizamos la generacion de numero de socio
    if not instance.empleado.num_legajo:
        max_num = int(Empleado.objects.all().aggregate(Max('num_legajo'))['num_legajo__max'])
        if max_num:
            instance.empleado.num_legajo = max_num + 1
        else:
            instance.empleado.num_legajo = 1
        instance.empleado.save()

@receiver(post_save, sender=Contrato)
def baja_contrato_anterior(instance, created, **kwargs):
    if created:
        empleado = instance.empleado
        for contrato in empleado.contratos.all().exclude(id=instance.id):
            if contrato.endda == date(9999, 12, 31):
                contrato.endda = instance.begda - timedelta(days=1)
                contrato.save()

#HACER PARA MOVER ENDDAS DE CONTRATOS VIEJOS
#HACER FUNCION PARA MOVER ENDDAS DE TURNOS VIEJOS