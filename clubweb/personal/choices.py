ESTADO_CIVIL = (
    (0, 'Sin Informar'),
    (1, 'Soltero'),
    (2, 'Concubinato'),
    (3, 'Casado'),
    (4, 'Divorciado'),
    (5, 'Viudo'),
)

NIVEL_EDUCATIVO = (
    (1, 'Educación Inicial'),
    (2, 'Educación Primaria'),
    (3, 'Educación Secundaria'),
    (4, 'Educación Superior'),
)

NIVEL_HABILIDAD = (
    (1, 'Bajo'),
    (2, 'Medio'),
    (3, 'Alto'),
    (4, 'Experto'),
)