#Imports django
from django.db import models
from django.contrib import admin
from django.forms import CheckboxSelectMultiple
#Imports del proyecto

#Imports de la app
from .models import Sector
from .models import TipoCargo, TipoDocumento
from .models import TipoHabilidad, Habilidad
from .models import Empleado, Contrato, Documento, Escolaridad, HabilidadAdquirida
from .models import Turno, Horario
from .models import Salario, Comportamiento, Concepto, Detalle
#Models Ocultos
def register_hidden_models(*model_names):
    for m in model_names:
        ma = type(
            str(m)+'Admin',
            (admin.ModelAdmin,),
            {
                'get_model_perms': lambda self, request: {}
            })
        admin.site.register(m, ma)

#Definimos Inlines
class ContratoInline(admin.TabularInline):
    model = Contrato
    extra = 0
    autocomplete_fields = ("unidad",)
    ordering = ("-endda",)

class DocumentoInline(admin.TabularInline):
    model = Documento
    extra = 0

class EscolaridadInline(admin.TabularInline):
    model = Escolaridad
    extra = 0

class HabilidadAdquiridaInline(admin.TabularInline):
    model = HabilidadAdquirida
    extra = 0
    autocomplete_fields = ("habilidad",)

class HorarioInline(admin.TabularInline):
    model = Horario
    extra = 0
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

class DetalleInline(admin.TabularInline):
    model = Detalle
    extra = 0

#Definimos Admins
class HabilidadAdmin(admin.ModelAdmin):
    model = Habilidad
    search_fields = ['nombre']
    list_filter = ['tipo']

class EmpleadoAdmin(admin.ModelAdmin):
    model = Empleado
    search_fields = ['nombres', 'apellidos']
    list_filter = ['contratos__unidad',]
    fields = ['num_legajo',
                ('num_doc', 'nombres', 'apellidos'),
                'fecha_nacimiento', 'estado_civil',
                'foto', 'email', 
                ('domicilio', 'telefono'),
                'usuario',]
    inlines = [ContratoInline, EscolaridadInline, DocumentoInline, HabilidadAdquiridaInline]

class TurnoAdmin(admin.ModelAdmin):
    model = Turno
    autocomplete_fields = ['empleado']
    search_fields = ['empleado__nombres', 'empleado__apellidos']
    list_filter = ['sector']
    fields = [('sector', 'empleado'),
                ('begda', 'endda'),]
    inlines = [HorarioInline]

class SalarioAdmin(admin.ModelAdmin):
    model = Salario
    autocomplete_fields = ['empleado']
    search_fields = ['empleado', 'contrato']
    list_filter = ['empleado__contratos__unidad',]
    inlines = [DetalleInline]

# Register your models here.
register_hidden_models(Sector, TipoCargo, TipoDocumento, TipoHabilidad)
register_hidden_models(Comportamiento, Concepto)
admin.site.register(Habilidad, HabilidadAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(Turno, TurnoAdmin)
admin.site.register(Salario, SalarioAdmin)