#Imports standard de python
from datetime import datetime
#Import Standard de django
from django import forms
from django.forms import ModelForm, modelformset_factory
#import extras:

#Import del Proyecto

#Imports de la app
from .models import Habilidad

#creamos ModelForm
class EmpleadoSearchForm(forms.Form):
    num_doc = forms.IntegerField(label='Num de Documento', required=False)
    num_legajo = forms.IntegerField(label='Numero de Legajo', required=False)
    widgets = {
            'num_doc': forms.TextInput(attrs={}),
            'num_legajo': forms.TextInput(attrs={}),
        }