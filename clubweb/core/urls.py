from django.conf.urls import url
from django.urls import path
#Import personales
from . import views

app_name = 'core'
urlpatterns = [
    #Basicas:
    url(r'^$', views.home, name='home'),
    path('faq/', views.faqs, name='faqs'),
    path('contacto/', views.contacto, name='contacto'),
    
    #Web Services
    path('ws/', views.ws_core, name='ws_core'),
    path('ws/<str:nombre_app>/<str:nombre_modelo>/', views.ws_core, name='ws_core'),

    #Task Manager:
    #path('task_progress/<str:queue_name>', tasks.task_progress, name='task_progress'),
]