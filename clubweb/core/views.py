#TAREAS POR HACER>
#   Busquedas en ModelAdmin, no debe distinguir acentos

#   CORE
#       Menu segun roles de usuario

#   SOCIOS
#       Pedido de Baja
#       Opcional de Pagos Atrasado con Intereses

#   COMERCIAL
#       Pagina de Recepcion de pedidos (Con cambios sobre el mismo)
#       Envio por mail a proveedores
#       Otros detalles de Ingresos Brutos
#       Api de AFIP

#   PERSONAL
#       Descarga de Archivo de Salarios Avanzado (Con conceptos por empleado l0> empleado o detalle)
#       Una vez realizado Cierre, no se puede modificar mas
#       Novedades (Ingresos y Egresos) > Calculo Mensual
#       Pagina de Fichado (Ingreso - Egreso)

#   Sistema de Comunicacion Masiva
#   Sistema para manejo de Background Tasks

#Imports standard de Python
import json
from django.utils import timezone
#Imports Standard de Django
from django.apps import apps
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
#Imports del proyecto
from noticias.models import Noticia
from calendario.models import Evento
#Imports de la app
from .models import Faq

# Create your views here.
def home(request):
    noticias = Noticia.objects.all().order_by('-fecha')[:3]
    eventos = Evento.objects.filter(fecha_fin__gt=timezone.now(), importante=True).order_by('-fecha_inicio')[:3]
    return render(request, 'home.html', {'eventos': eventos, 'noticias': noticias,})

def faqs(request):
    faqs = Faq.objects.all().order_by('orden')
    eventos = Evento.objects.filter(fecha_fin__gt=timezone.now(), importante=True).order_by('-fecha_inicio')[:3]
    return render(request, 'faq.html', {'eventos': eventos, 'faqs': faqs, })

def contacto(request):
    eventos = Evento.objects.filter(fecha_fin__gt=timezone.now(), importante=True).order_by('-fecha_inicio')[:3]
    return render(request, 'contacto.html', {'eventos': eventos, })

@staff_member_required
def ws_core(request, nombre_app=None, nombre_modelo=None):
    if nombre_app and nombre_modelo:
        if apps.all_models[nombre_app][nombre_modelo.lower()]:
            modelo = apps.all_models[nombre_app][nombre_modelo.lower()]
            if hasattr(modelo, 'as_dict'):
                datos = modelo.objects.all()
                datos = [d.as_dict() for d in datos]
                return HttpResponse(json.dumps({nombre_modelo+'s': datos, "cant_registros": len(datos),}), content_type='application/json')
    #Generamos todas las apps con webservices
    apps_listas = {}
    for app, models in apps.all_models.items():
        for model in models.values():
            if hasattr(model, 'as_dict'):
                if app in apps_listas:
                    apps_listas[app].append(model._meta.model_name)
                else:
                    apps_listas[app] = [model._meta.model_name, ]
    return render(request, 'extras/ws.html', {"apps_listas": apps_listas,})