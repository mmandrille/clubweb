from django.conf.urls import url
from django.urls import path
#Import personales
from . import views

app_name = 'comercio'
urlpatterns = [
    #Basicas:
    path('', views.comercio_menu, name='socios_menu'),
    #Facturacion
    path('crear_factura', views.crear_factura, name='crear_factura'),
    path('crear_factura/<int:id_factura>', views.crear_factura, name='crear_factura'),
    path('confirmar_factura', views.confirmar_factura, name='confirmar_factura'),
    path('ver_factura/<int:id_factura>', views.ver_factura, name='ver_factura'),
    #Pedidos
    path('crear_producto', views.crear_producto, name='crear_producto'),
    path('buscar_pedido', views.buscar_pedido, name='buscar_pedido'),
    path('pedido/<int:id_pedido>', views.ver_pedido, name='ver_pedido'),
    path('accion_pedido', views.ver_pedido, name='ver_pedido'),
    #Productos:
    path('eliminar/<int:id_producto>', views.confirmar_eliminacion, name='confirmar_eliminacion'),
    path('eliminar/confirmado/<int:id_producto>', views.eliminar_producto, name='eliminar_producto'),
    #reportes
    path('inventario', views.inventario, name='inventario'),
    #Automatizacion
    path('indexar', views.indexar, name='indexar'),
]