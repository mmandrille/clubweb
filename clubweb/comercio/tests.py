#Imports Python
import random
#Import Django
from django.test import TestCase
#Imports de la app
from .models import Rubro, Producto, Stock

# Create your tests here.
def crear_item(rubro, nombre):
    rubro = Rubro.objects.get_or_create(nombre=rubro)[0]
    prod = Producto(rubro=rubro, codigo=random.randrange(1000000,9999999), nombre=nombre, costo=random.randrange(1,20), precio_venta=random.randrange(21,30))
    prod.save()
    stock = Stock(producto=prod, cantidad=random.randrange(10,50), cant_ideal=random.choice([50, 60, 70, 80]), cant_min=10)
    stock.save()

def cargar_inventario():
    Rubro.objects.all().delete()
    crear_item('Comestibles', 'ACEITE MEZCLA 1lt')
    crear_item('Comestibles', 'ARROZ')
    crear_item('Comestibles', 'LATA ARVEJA')
    crear_item('Comestibles', 'LATA CHOCLO CREMOSO')
    crear_item('Comestibles', 'LATA DURAZNO AL NATURAL')
    crear_item('Comestibles', 'FIDEOS - CODITOS 1kg')
    crear_item('Comestibles', 'POROTOS 1kg')
    crear_item('Comestibles', 'YERBA CBSÉ CON HIERBAS')
    crear_item('Comestibles', 'YERBA MATE CON PALO')
    crear_item('Comestibles', 'BONDIOLA x 100gr')
    crear_item('Comestibles', 'JAMÓN COCIDO x 100gr')
    crear_item('Comestibles', 'MORTADELA x 100gr')
    crear_item('Comestibles', 'SALCHICHA x 6u')
    crear_item('Comestibles', 'CREMA DE LECHE 250gr')
    crear_item('Comestibles', 'DULCE DE LECHE REPOSTERO 500gr')
    crear_item('Bebidas', 'Paso de Los Toros 1lt')
    crear_item('Bebidas', 'Paso de Los Toros 1.5lt')
    crear_item('Bebidas', 'Paso de Los Toros 2lt')
    crear_item('Bebidas', 'Coca Cola 1lt')
    crear_item('Bebidas', 'Coca Cola 2lt')
    crear_item('Bebidas', 'Fernet 1lt')
    crear_item('Bebidas', 'Vino Toro Tinto (Botella)')
    crear_item('Bebidas', 'Vino Toro Blanco (Botella)')
    crear_item('Bebidas', 'Vino Toro Tinto (Cajita)')
    crear_item('Bebidas', 'Vino Toro Tinto (Cajita)')
    crear_item('Pesca', 'Nylon 0.25mm 100mts')
    crear_item('Pesca', 'Nylon 0.30mm 100mts')
    crear_item('Pesca', 'Nylon 0.35mm 100mts')
    crear_item('Pesca', 'Nylon 0.40mm 100mts')
    crear_item('Pesca', 'Nylon 0.50mm 100mts')
    crear_item('Pesca', 'Leader Acero x 5u.')
    crear_item('Pesca', 'Anzuelo #4 x 10u')
    crear_item('Pesca', 'Anzuelo #3 x 10u')
    crear_item('Pesca', 'Anzuelo #2 x 10u')
    crear_item('Pesca', 'Anzuelo #1 x 10u')
    crear_item('Pesca', 'Anzuelo #0 x 10u')
    crear_item('Pesca', 'Plomada Corrediza 10gr x 20u')
    crear_item('Pesca', 'Plomada Corrediza 20gr x 10u')
    crear_item('Pesca', 'Plomada Corrediza 30gr x 5u')
    crear_item('Pesca', 'Plomada Perita 10gr x 20u')
    crear_item('Pesca', 'Plomada Perita 20gr x 10u')
    crear_item('Carnada', 'Tripa de Sabalo (Bolsa)')
    crear_item('Carnada', 'Morenas x12')


