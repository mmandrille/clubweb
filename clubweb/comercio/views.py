#Imports de Django
from datetime import date
from django.shortcuts import render
#Imports de la app
from .models import Producto, Stock
from .models import Pedido, Requerimiento, EventoPedido
from .models import Factura, Cliente, Detalle
from .forms import FacturaForm, DetalleFormset, ClienteForm, BuscarPedido
from .functions import alertas

# Create your views here.
def comercio_menu(request):
    return render(request, "menu_comercio.html", {'alertas': alertas(), })

def ver_factura(request, id_factura):
    factura = Factura.objects.get(id=id_factura)
    return render(request, "ver_factura.html", {'factura': factura, })

def confirmar_factura(request, factura=None, cliente=None, detalles=None):
    if factura:
        return render(request, "comprobar_factura.html", {
            'factura': factura, 
            'cliente': cliente,
            'detalles': detalles,
            'total': sum([d.subtotal() for d in detalles])
        })
    else:
        #Creamos la factura:
        factura = Factura()
        if not Factura.objects.filter(num_factura=request.POST['factura_num']):
            factura.num_factura = request.POST['factura_num']
        factura.fecha = request.POST['factura_fecha']
        factura.save()
        #Creamos el cliente:
        cliente = Cliente()
        cliente.factura = factura
        cliente.nombre = request.POST['cliente_nombre']
        if cliente.nombre != "Consumidor Final":
            if request.POST['cliente_num_doc']:
                cliente.num_doc = request.POST['cliente_num_doc']
            cliente.telefono = request.POST['cliente_telefono']
            cliente.email = request.POST['cliente_email']
            cliente.save()
        #Creamos Detalles:
        for key, value in request.POST.items():
            if key[0:7] == "detalle" and key[-8:] == "producto":
                detalle = Detalle()
                detalle.factura = factura
                detalle.producto = Producto.objects.get(id=value)
                detalle.cantidad = int(request.POST[key[:-8]+'cantidad'])
                detalle.save()
        return ver_factura(request, factura.id)

def crear_factura(request, id_factura=None):
    if request.method == "GET":
        if id_factura:
            factura = Factura.objects.get(id=id_factura)
            form_factura = FacturaForm(instance=factura)
            form_cliente = ClienteForm(instance=factura.cliente)
            formset_detalle = DetalleFormset(instance=factura)
        else:
            form_factura = FacturaForm()
            form_cliente = ClienteForm()
            formset_detalle = DetalleFormset()
        
        return render(request, "crear_factura.html", {
            'form_factura': form_factura,
            'form_cliente': form_cliente,
            'formset_detalle': formset_detalle,
            'productos': Producto.objects.all(),
        })
    else:
        form_factura = FacturaForm(request.POST)
        form_cliente = ClienteForm(request.POST)
        formset_detalle = DetalleFormset(request.POST)
        if form_factura.is_valid() and form_cliente.is_valid() and formset_detalle.is_valid():
            #Factura
            factura = Factura()
            factura.num_factura = form_factura.cleaned_data['num_factura']
            factura.fecha = form_factura.cleaned_data['fecha']
            #Cliente
            cliente = form_cliente.save(commit=False)
            cliente.factura = factura
            #Detalle
            detalles = []
            for key, value in request.POST.items():
                if key[0:8] == "detalles" and key[-8:] == "producto" and value:
                    detalle = Detalle()
                    detalle.factura = factura
                    detalle.producto = Producto.objects.get(id=int(value))
                    detalle.cantidad = int(request.POST[key[:-8]+'cantidad'])
                    detalles.append(detalle)
            return confirmar_factura(request, factura=factura, cliente=cliente, detalles=detalles)  
        else:
            return render(request, "crear_factura.html", {
                'form_factura': FacturaForm(request.POST),
                'form_cliente': FacturaForm(request.POST),
                'formset_detalle': FacturaForm(request.POST),
                'productos': Producto.objects.all(),
            })

def inventario(request):
    items = Stock.objects.all()
    for item in items:
        if item.cantidad <= item.cant_min:
            item.alerta = True
    return render(request, "inventario.html", {'items': items, })

def confirmar_eliminacion(request, id_producto):
    producto = Producto.objects.get(pk= id_producto)
    return render(request, "eliminar_producto.html", {'producto': producto, })

def eliminar_producto(request, id_producto):
    Producto.objects.get(pk=id_producto).delete()
    return inventario(request)

def ver_pedido(request, id_pedido=None):
    if id_pedido:
        pedido = Pedido.objects.filter(pk=id_pedido).first()
        if pedido:
            return render(request, "ver_pedido.html", {'pedido': pedido, })
        else:
            return buscar_pedido(request)
    
    if request.method == "POST":
        pedido = Pedido.objects.get(pk=request.POST['pedido'])
        for key, value in request.POST.items():
            if key[0:13] == "requerimiento" and key[-2:] == 'id':
                index = key.split('-')[1]
                #Actualizamos el costo del producto
                producto = Producto.objects.get(id=value)
                producto.costo = request.POST['requerimiento-'+index+'-precio']
                producto.save()
                #Actualizamos la cantidad del pedido
                requerimiento = Requerimiento.objects.get(pedido=pedido, producto=producto)
                requerimiento.cantidad = request.POST['requerimiento-'+index+'-cantidad']
                requerimiento.save()
                #Agregamos un evento de modificacion
                evento = EventoPedido()
                evento.pedido = pedido
                evento.tipo = "M"
                evento.fecha = date.today()
                evento.save()
        if 'recibir' in request.POST:
            pedido.eventos.filter(tipo='E').delete()
            evento = EventoPedido()
            evento.pedido = pedido
            evento.tipo = 'E'
            evento.fecha = date.today()
            evento.save()
        if 'pagar' in request.POST:
            pedido.eventos.filter(tipo='P').delete()
            evento = EventoPedido()
            evento.pedido = pedido
            evento.tipo = 'P'
            evento.fecha = date.today()
            evento.save()

        return render(request, "ver_pedido.html", {'pedido': pedido, })

def buscar_pedido(request):
    message = ''
    form = BuscarPedido()
    if request.method == "POST":
        form = BuscarPedido(request.POST)
        if form.is_valid():
            if form.cleaned_data['num_pedido'] is not None:
                pedidos = Pedido.objects.filter(pk=form.cleaned_data['num_pedido'])
            elif form.cleaned_data['proveedor'] is not None:
                pedidos = Pedido.objects.filter(proveedor=form.cleaned_data['proveedor'])
            elif form.cleaned_data['producto'] is not None:
                pedidos = Pedido.objects.filter(requerimientos__producto=form.cleaned_data['producto'])
            pedido = pedidos.exclude(eventos__tipo='P').first()
            if pedido:
                return ver_pedido(request, pedido.id)
            else:
                message = "No existe pedido para estos parametros"
    return render(request, 'buscar_pedido.html', {
            'titulo': 'Busqueda de Pedidos Activos',
            'form': form,
            'message': message,
            'boton': 'Buscar',
            'alertas': alertas(),
    })

def indexar(request):
    return render(request, "menu_comercio.html", {})

def crear_producto(request):

    return render(request, "crear_producto.html", {})