#Imports del framework Django
from django import forms
from django.db.models import Max
from django.forms import ModelChoiceField, ModelForm
from django.forms import formset_factory, inlineformset_factory
from django.core.exceptions import ValidationError
#Imports de la app:
from .models import Proveedor, Producto, Factura, Detalle, Cliente, Pedido

#Definimos los forms
class BuscarPedido(forms.Form):
    num_pedido = forms.IntegerField(label='Num de Pedido', required=False)
    proveedor = ModelChoiceField(queryset=Proveedor.objects.filter(activo=True), required=False)
    producto = ModelChoiceField(queryset=Producto.objects.all(), required=False)
    def clean(self):
        if not (self.cleaned_data.get('proveedor') 
            or self.cleaned_data.get('producto') 
            or self.cleaned_data.get('num_pedido')
        ):
            raise ValidationError({'producto': ('Debe Seleccionarse al menos una opcion.')})

class FacturaForm(ModelForm):
    class Meta:
        model = Factura
        fields= '__all__'
        exclude = ('pagada',)
    def __init__(self, *args, **kwargs):
        super(FacturaForm, self).__init__(*args, **kwargs)
        if not self.fields['num_factura'].initial:
            max_num = int(Factura.objects.all().aggregate(Max('num_factura'))['num_factura__max'])
            if max_num:
                self.fields['num_factura'].initial = max_num + 1
            else:
                self.fields['num_factura'].initial = 1

class ClienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields= '__all__'
        exclude = ('factura',)
    def __init__(self, *args, **kwargs):
        super(ClienteForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].required = False

class DetalleForm(ModelForm):
    class Meta:
        model = Detalle
        fields = ('producto', 'cantidad')
    def __init__(self, *args, **kwargs):
        super(DetalleForm, self).__init__(*args, **kwargs)
        self.fields['producto'].required = False
        self.fields['cantidad'].required = True

DetalleFormset = inlineformset_factory(Factura, Detalle,
            can_delete=False, 
            fields= '__all__',
            extra = 5,
        )