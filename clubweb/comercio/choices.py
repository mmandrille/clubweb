#Choice Fields
ESTADO_PEDIDO = (
    (1, 'Automaticamente Generado'),
    (2, 'Aprobado'),
    (3, 'Pedido'),
    (4, 'Entregado'),
)

EVENTO_PEDIDO = (
    ('C', 'Creado'),
    ('M', 'Modificado'),
    ('E', 'Entregado'),
    ('P', 'Pagado'),
)