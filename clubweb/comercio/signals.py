#Imports de python
from datetime import datetime, date, timedelta
#Imports de Django
from django.dispatch import receiver
from django.db.models import Max
from django.db.models.signals import pre_save, post_save

#Imports de la app
from .models import Producto, Stock
from .models import Factura, Detalle
from .models import Pedido, Requerimiento, EventoPedido

#Definimos nuestra señales
@receiver(post_save, sender=Producto)
def crear_stock(instance, created, **kwargs):
    if created:
        stock = Stock()
        stock.producto = instance
        stock.save()

@receiver(post_save, sender=Factura)
def generar_num_factura(instance, **kwargs):
    if not instance.num_factura:
        max_num = int(Factura.objects.all().aggregate(Max('num_factura'))['num_factura__max'])
        if max_num:
            instance.num_factura = max_num + 1
        else:
            instance.num_factura = 1
        instance.save()

@receiver(pre_save, sender=Detalle)
def reducir_existencia(instance, **kwargs):
    stock = Stock.objects.get(producto__id=instance.producto.id)
    if instance.id is None: #Si la factura es nueva
        stock.cantidad-= instance.cantidad
    else:#Si se modifico una factura
        stock.cantidad+= Detalle.objects.get(id=instance.id).cantidad - instance.cantidad
    stock.save()

@receiver(post_save, sender=Stock)
def generar_pedido(instance, **kwargs):
    if instance.cantidad <= instance.cant_min:
        if not Requerimiento.objects.filter(producto=instance.producto, pedido__eventos__tipo='E'):
            #Hay que agregar el requerimiento
            req = Requerimiento()
            #Lo agregamos a un pedido o generamos uno nuevo
            if hasattr(instance.producto, 'proveedor'):
                proveedor = instance.producto.proveedor
            if proveedor:
                pedido = proveedor.pedidos.exclude(eventos__tipo='E').first()
            else:
                pedido = Pedido.objects.filter(proveedor__isnull=True).exclude(eventos__tipo='E').first()
            if not pedido:
                pedido = Pedido()
                pedido.proveedor = proveedor
                pedido.save()
            #Asignamos el producto
            req.pedido = pedido
            req.producto = instance.producto
            req.cantidad = instance.cant_ideal - instance.cantidad
            req.save()

@receiver(post_save, sender=Pedido)
def generar_evento(instance, created, **kwargs):
    if created:
        EventoPedido(pedido=instance, tipo='C', fecha=datetime.today()).save()

@receiver(post_save, sender=EventoPedido)
def aumentar_existencia(instance, created, **kwargs):
    if created and instance.tipo == 'E':#Se genera el evento de entregado Entregado
        for req in instance.pedido.requerimientos.all():
            stock = Stock.objects.get(producto=req.producto)
            stock.cantidad+= req.cantidad
            stock.save()