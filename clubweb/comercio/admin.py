#Imports Django
from django.db import models
from django.contrib import admin
from django.forms import TextInput
#Imports de la app
from .models import Rubro, Proveedor, Producto, Stock
from .models import Pedido, Requerimiento, EventoPedido
from .models import Factura, Cliente, Detalle

#Models Ocultos
def register_hidden_models(*model_names):
    for m in model_names:
        ma = type(
            str(m)+'Admin',
            (admin.ModelAdmin,),
            {
                'get_model_perms': lambda self, request: {}
            })
        admin.site.register(m, ma)

#Definimos inlines:
class ClienteInline(admin.TabularInline):
    model = Cliente
    search_fields = ['nombre']
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
    }
    extra = 0

class DetalleInline(admin.TabularInline):
    model = Detalle
    autocomplete_fields = ['producto',]
    extra = 1

class RequerimientoInline(admin.TabularInline):
    model = Requerimiento
    extra = 1

class EventoPedidoInline(admin.TabularInline):
    model = EventoPedido
    extra = 0

class ProductoInline(admin.TabularInline):
    model = Producto
    extra = 1

class StockInline(admin.TabularInline):
    model = Stock

#Definimos admins
class ProductoAdmin(admin.ModelAdmin):
    model = Producto
    search_fields = ['nombre',]
    list_filter = ['rubro']
    inlines = [StockInline]
    readonly_fields = ['codigo']

class StockAdmin(admin.ModelAdmin):
    model = Stock
    search_fields = ['producto__nombre']
    list_filter = ['producto__rubro']

class FacturaAdmin(admin.ModelAdmin):
    model = Factura
    search_fields = ['cliente__nombre', 'detalles__producto__nombre']
    list_filter = ['pagada', 'fecha']
    inlines = [ClienteInline, DetalleInline]

class ProveedorAdmin(admin.ModelAdmin):
    model = Proveedor
    search_fields = ['nombre']
    inlines = [ProductoInline]

class PedidoAdmin(admin.ModelAdmin):
    model = Pedido
    search_fields = ['proveedor__nombre', 'requerimientos__producto__nombre']
    list_filter = ['eventos__tipo']
    inlines = [RequerimientoInline, EventoPedidoInline]


# Register your models here.
register_hidden_models(Rubro,)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(Factura, FacturaAdmin)
admin.site.register(Proveedor, ProveedorAdmin)
admin.site.register(Pedido, PedidoAdmin)