#Imports de python
#Imports django
#Import de proyecto
from finanzas.models import Asiento
#Imports de la app
from .models import Factura, Pedido

#Definimos nuestras funciones informativas
def ingresos(begda, endda):
    #FACTURA PAGADA
    movimentos = []
    facturas = Factura.objects.filter(fecha__range=(begda, endda), pagada=True)
    for factura in facturas:
        mov = Asiento()
        mov.tipo = 'IN'
        mov.origen = "Comercio"
        mov.fecha = factura.fecha
        mov.monto = factura.total()
        mov.descripcion = 'Factura: ' + str(factura.num_factura) + ' $' + str(factura.total()) + '. Fecha:'+str(factura.fecha)
        movimentos.append(mov)
    return movimentos

def egresos(begda, endda):
    #PEDIDO PAGADO
    movimentos = []
    pedidos = Pedido.objects.filter(eventos__fecha__range=(begda, endda), eventos__tipo='P')
    for pedido in pedidos:
        mov = Asiento()
        mov.tipo = 'EG'
        mov.origen = "Comercio"
        mov.fecha = pedido.eventos.get(tipo='P').fecha
        mov.monto = pedido.total()
        mov.descripcion = 'Pago Pedido a ' + str(pedido.proveedor) + '. Fecha Pago:'+str(mov.fecha)
        movimentos.append(mov)
        if pedido.eventos.get(tipo='E').fecha < begda:
            mov = Asiento()
            mov.tipo = 'DP'
            mov.origen = "Comercio"
            mov.fecha = pedido.eventos.get(tipo='E').fecha
            mov.monto = - pedido.total()
            mov.descripcion = 'Cancelamos deuda por Pedido a ' + str(pedido.proveedor) + '. Fecha Entrega:'+str(mov.fecha)
            movimentos.append(mov)
    return movimentos

def por_cobrar(begda, endda):
    #FACTURA SIN PAGAR
    movimentos = []
    facturas = Factura.objects.filter(fecha__range=(begda, endda), pagada=False)
    for factura in facturas:
        mov = Asiento()
        mov.tipo = 'DC'
        mov.origen = "Comercio"
        mov.fecha = factura.fecha
        mov.monto = factura.total()
        if hasattr(factura, 'cliente'):
            mov.descripcion = 'Factura Adeudada: ' + str(factura.num_factura) + ' de ' + str(factura.cliente) + ' por $' + str(factura.total()) + '. Fecha:'+str(factura.fecha)
        else:
            mov.descripcion = 'Factura Adeudada: ' + str(factura.num_factura) + ' SIN REGISTRAR CLIENTE por $' + str(factura.total()) + '. Fecha:'+str(factura.fecha)
        movimentos.append(mov)
    return movimentos

def por_pagar(begda, endda):
    #PEDIDO ENTREGADO SIN PAGAR
    movimentos = []
    pedidos = Pedido.objects.filter(eventos__fecha__range=(begda, endda), eventos__tipo='E').exclude(eventos__tipo='P')
    for pedido in pedidos:
        mov = Asiento()
        mov.tipo = 'DP'
        mov.origen = "Comercio"
        mov.fecha = pedido.eventos.get(tipo='E').fecha
        mov.monto = pedido.total()
        mov.descripcion = 'Adeudado por Pedido a ' + str(pedido.proveedor) + '. Fecha Entrega:'+str(mov.fecha)
        movimentos.append(mov)
    return movimentos
    
def recoleccion(begda, endda):
    movimientos = ingresos(begda, endda)
    movimientos+= egresos(begda, endda)
    movimientos+= por_cobrar(begda, endda)
    movimientos+= por_pagar(begda, endda)
    return movimientos