#imports de Python
from datetime import datetime
#Import de Django
from django.db import models
from django.contrib.auth.models import User
#Imports Extras
from tinymce.models import HTMLField
#Imports del proyecto

#Imports de la app
from .functions import validar_cuit
from .choices import ESTADO_PEDIDO, EVENTO_PEDIDO

#Types Models
class Rubro(models.Model):
    nombre = models.CharField(verbose_name="Nombre", max_length=50)
    def __str__(self):
        return self.nombre

# Create your models here.
class Proveedor(models.Model):
    cuit = models.CharField('Cuit', max_length=13, default="XX-XXXXXXXX-X", validators=[validar_cuit])
    nombre = models.CharField(verbose_name="Nombre", max_length=100, null=True, blank=True)
    telefono = models.CharField('Telefono', max_length=15, blank=True, null=True)
    email = models.EmailField('Correo Electronico', null=True, blank=True)
    activo = models.BooleanField(verbose_name="Activo", default=True)
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="proveedores")
    class Meta:
        verbose_name_plural = 'Proveedores'
    def __str__(self):
        return self.nombre

class Producto(models.Model):
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE, related_name="productos", null=True, blank=True)
    rubro = models.ForeignKey(Rubro, on_delete=models.CASCADE, related_name="productos", null=True, blank=True)
    codigo = models.IntegerField(null=True, blank=True)
    nombre = models.CharField(verbose_name="Nombre", max_length=50)
    costo = models.DecimalField(verbose_name="Costo Unitario", max_digits=8, decimal_places=2)
    precio_venta = models.DecimalField(verbose_name="Precio de Venta", max_digits=8, decimal_places=2)
    class Meta:
        unique_together = [('proveedor', 'codigo')]
    def __str__(self):
        return self.rubro.nombre + ' | ' + self.nombre + ' $' + str(self.precio_venta)

class Stock(models.Model):
    producto = models.OneToOneField(Producto, on_delete=models.CASCADE, related_name="stock")
    cantidad = models.IntegerField(verbose_name="Cantidad", default=0)
    cant_ideal = models.IntegerField(verbose_name="Cantidad Ideal", default=0)
    cant_min = models.IntegerField(verbose_name="Cantidad Minima", default=0)
    def __str__(self):
        return self.producto.nombre + ': ' + str(self.cantidad)

class Pedido(models.Model):
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE, related_name="pedidos", null=True, blank=True)
    importe = models.DecimalField(verbose_name="Importe de Factura", max_digits=8, decimal_places=2, default=0)
    class Meta:
        verbose_name_plural = 'Pedidos'
    def creacion(self):
        return self.eventos.get(tipo='C').fecha
    def entregado(self):
        if self.eventos.filter(tipo='E'):
            return self.eventos.get(tipo='E').fecha
        else:
            return None
    def pagado(self):
        if self.eventos.filter(tipo='P'):
            return self.eventos.get(tipo='P').fecha
        else:
            return None
    def __str__(self):
        if self.proveedor:
            return self.proveedor.nombre + ': ' + str(self.creacion())
        else:
            return 'Pedido Sin Proveedor: ' + str(self.creacion())
    def total(self):
        if self.importe:
            return self.importe
        else:
            return sum(r.total() for r in self.requerimientos.all())

class Requerimiento(models.Model):
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE, related_name="requerimientos")
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, related_name="requerimientos")
    cantidad = models.IntegerField(verbose_name="Cantidad")
    def __str__(self):
        return self.producto.nombre + ': ' + str(self.cantidad)
    def total(self):
        return self.cantidad * self.producto.costo

class EventoPedido(models.Model):
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE, related_name="eventos")
    tipo = models.CharField('Tipo de Evento', max_length=1, choices=EVENTO_PEDIDO)
    fecha = models.DateField(verbose_name="Fecha Evento")
    def __str__(self):
        return str(self.pedido) + ' ' + self.get_tipo_display() + ' el ' + str(self.fecha)

class Factura(models.Model):
    num_factura = models.IntegerField(verbose_name="N de Factura", unique=True, null=True, blank=True)
    fecha = models.DateField(verbose_name="Fecha", default=datetime.today)
    pagada = models.BooleanField(verbose_name="Pagada", default=True)
    def total(self):
        return sum(d.subtotal() for d in self.detalles.all())
    def __str__(self):
        return str(self.num_factura) + ': $' + str(self.total())

class Cliente(models.Model):
    factura = models.OneToOneField(Factura, on_delete=models.CASCADE)
    nombre = models.CharField(verbose_name="Nombre y Apellido", max_length=100, default="Consumidor Final")
    num_doc = models.IntegerField('Num de Documento', null=True, blank=True)
    telefono = models.CharField('Telefono', max_length=15, blank=True, null=True)
    email = models.EmailField('Correo Electronico', null=True, blank=True)
    def __str__(self):
        return self.nombre

class Detalle(models.Model):
    factura = models.ForeignKey(Factura, on_delete=models.CASCADE, related_name="detalles")
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE, related_name="detalles")
    cantidad = models.IntegerField(verbose_name="Cantidad")
    def __str__(self):
        return self.producto.nombre + ': ' + str(self.cantidad)
    def subtotal(self):
        return self.cantidad * self.producto.precio_venta

#Definimos nuestras signals
from .signals import crear_stock#Al cargar un nuevo producto, generamos stock 0
from .signals import generar_num_factura
from .signals import reducir_existencia
from .signals import generar_pedido
from .signals import generar_evento
from .signals import aumentar_existencia
