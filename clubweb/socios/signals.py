#Imports de Python

#Imports de Django
from django.dispatch import receiver
from django.db.models import Max
from django.db.models.signals import post_save

#Imports de la app
from .models import Socio, EventoSocio

#Definimos nuestra señales
@receiver(post_save, sender=Socio)
def nuevo_socio(instance, **kwargs):
    #Automatizamos la generacion de numero de socio
    if not instance.num_socio:
        #Obtenemos numero maximo
        max_num = Socio.objects.all().aggregate(Max('num_socio'))['num_socio__max']
        if max_num:
            instance.num_socio = max_num + 1
        else:
            instance.num_socio = 1
        instance.save()
    #Si no tiene eventos Generamos el alta:
    if not instance.eventos.all():
        evento = EventoSocio()
        evento.socio = instance
        evento.tipo = 'IN'
        evento.save()
