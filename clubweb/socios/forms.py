#Imports standard de python
from datetime import datetime
#Import Standard de django
from django import forms
from django.forms import ModelForm
#import extras:

#Import Personales
from .models import Socio, Pago

#Definimos
DATE_INPUT_FORMATS = ['%d-%m-%Y']

#creamos ModelForm
class SocioSearchForm(forms.Form):
    num_doc = forms.IntegerField(label='Num de Documento', required=False)
    num_socio = forms.IntegerField(label='Numero de Socio', required=False)
    widgets = {
            'num_doc': forms.TextInput(attrs={}),
            'num_socio': forms.TextInput(attrs={}),
        }

class InscriptoForm(ModelForm):
    telefono = forms.CharField(initial="+54 342 ")
    class Meta:
        model = Socio
        fields = ['tipo_doc', 'num_doc', 'nombres', 'apellidos', 'fecha_nacimiento', 'telefono', 'email']
        widgets = {
            'num_doc': forms.TextInput(attrs={}),
            'fecha_nacimiento': forms.SelectDateWidget(years = reversed(range(1900, datetime.today().year+1))),
        }
    def __init__(self, *args, **kwargs):
        self.nolabel = ['tipo_doc', 'num_doc']
        self.alinear = [('tipo_doc', 'num_doc'), ]
        super(InscriptoForm, self).__init__(*args, **kwargs)
        
        
class PagoForm(ModelForm):
    num_socio = forms.IntegerField(label='Numero de Socio')
    num_doc = forms.IntegerField(label='Num de Documento')
    ano_cuota = forms.ChoiceField(label='Año', 
        choices=[(y,y) for y in range(2019, datetime.today().year+1)],
        initial=datetime.now().year,
    )
    mes_cuota = forms.ChoiceField(label='Mes', 
        choices=[(m,m) for m in range(1,13)], 
        initial=datetime.now().month,
    )
    class Meta:
        model = Pago
        fields = ['ano_cuota', 'mes_cuota', 'num_socio', 'num_doc', 'tipo_pago', 'fecha_pago', 'monto_pagado', 'comprobante']

        widgets = {
            'num_socio': forms.TextInput(attrs={}),
            'num_doc': forms.TextInput(attrs={}),
        }
    def __init__(self, *args, **kwargs):
        self.alinear = [('ano_cuota', 'mes_cuota'), ]
        super(PagoForm, self).__init__(*args, **kwargs)

class PeriodoForm(forms.Form):
    begda_month = forms.ChoiceField(label='Mes Inicio', choices=[(m,m) for m in range(1,13)], initial=datetime.now().month)
    begda_year = forms.ChoiceField(label='Año Inicio', choices=[(y,y) for y in range(2019, datetime.today().year+1)], initial=datetime.now().year)
    endda_month = forms.ChoiceField(label='Mes Fin', choices=[(m,m) for m in range(1,13)], initial=datetime.now().month)
    endda_year = forms.ChoiceField(label='Año Fin', choices=[(y,y) for y in range(2019, datetime.today().year+1)], initial=datetime.now().year)

class MesForm(forms.Form):
    month = forms.ChoiceField(label='Mes', choices=[(m,m) for m in range(1,13)], initial=datetime.now().month)
    year = forms.ChoiceField(label='Año', choices=[(y,y) for y in range(2019, datetime.today().year+1)], initial=datetime.now().year)
    def __init__(self, *args, **kwargs):
        self.alinear = [('month', 'year'), ]
        super(MesForm, self).__init__(*args, **kwargs)

class UploadCsv(forms.Form):
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))