from django.conf.urls import url
from django.urls import path
#Import personales
from . import views

app_name = 'socios'
urlpatterns = [
    #Basicas:
    path('', views.socios_menu, name='socios_menu'),

    path('inscribite', views.inscribite, name='inscribite'),
    path('pago', views.informar_pago, name='informar_pago'),
    
    #Informacion
    path('buscar', views.resumen, name='resumen'),
    path('<int:id_socio>', views.resumen, name='resumen'),
    path('tarjeta/<int:id_socio>', views.tarjeta, name='tarjeta'),
    path('buscador', views.buscador, name='buscador'),

    #reportes
    path('generar_cuotas', views.generar_cuotas, name='generar_cuotas'),
    path('ver_cuotas/<int:year>/<int:month>', views.ver_cuotas, name='ver_cuotas'),
    path('delete_cuotas/<int:year>/<int:month>', views.delete_cuotas, name='delete_cuotas'),
    path('facturacion', views.informe_facturacion, name='informe_facturacion'),
    path('deudores', views.deudores, name='deudores'),
    #Managment
    path('upload', views.upload_socios, name='upload_socios'),
]