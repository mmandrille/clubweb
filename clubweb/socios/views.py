#Imports de Python
import calendar
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
#Import Standard de Django
from django.shortcuts import render
from django.db.models import Q
#decoradores
from django.contrib.admin.views.decorators import staff_member_required
#Imports del proyecto
from georef.models import Localidad
#Import de la app
from .models import TipoSocio, Socio, EventoSocio, Domicilio, Telefono
from .models import Cuota
from .forms import SocioSearchForm, InscriptoForm, PagoForm, PeriodoForm, UploadCsv, MesForm
from .functions import periodos_generados

# Create your views here.
def socios_menu(request):
    return render(request, "menu_socios.html", {'periodos': periodos_generados(), })

def resumen(request, id_socio=None):
    if id_socio:
        try:
            socio = Socio.objects.get(pk=id_socio)
            return render(request, 'socio_resumen.html', {'socio': socio,})
        except Socio.DoesNotExist:
            return render(request, 'extras/error.html', {})
    else:
        if request.method == "GET":
            form = SocioSearchForm()
            return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Resumen del Socio', 'boton': 'Buscar'})
        else:
            form = SocioSearchForm(request.POST)
            if form.is_valid():
                try:
                    if form.cleaned_data['num_doc']:
                        socio = Socio.objects.get(num_doc=form.cleaned_data['num_doc'])
                    elif form.cleaned_data['num_socio']:
                        socio = Socio.objects.get(num_socio=form.cleaned_data['num_socio'])
                except Socio.DoesNotExist:
                    return render(request, 'extras/error.html', {})
                return resumen(request, socio.id)
            else:
                return render(request, 'extras/error.html', {})

def buscador(request):
    if request.method == 'POST':
        if 'buscar' in request.POST:
            buscar = request.POST['buscar']
            socios = Socio.objects.filter(Q(nombres__icontains=buscar)
                    | Q(apellidos__icontains=buscar)
            )
        return render(request, 'lista_socios.html', {'socios': socios, })
    else:
        return resumen(request)

def tarjeta(request, id_socio):
    try:
        socio = Socio.objects.get(pk=id_socio)
        return render(request, 'tarjeta.html', {'socio': socio,})
    except Socio.DoesNotExist:
        return render(request, 'extras/error.html', {})

def inscribite(request):
    if request.method == "GET":
        form = InscriptoForm()
        return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Inscripcion de Socios', 'boton': 'Inscribite'})
    else:
        form = InscriptoForm(request.POST, request.FILES)
        if form.is_valid():
            titulo = 'Inscripcion de Nuevo Socio'
            form.save()
            #Obtenemos telefono
            tel = form.cleaned_data['telefono']
            telefono = Telefono()
            telefono.socio = form.instance
            if len(tel.split(' ')) == 3:
                telefono.prefijo = tel.split(' ')[0]
                telefono.cod_area = tel.split(' ')[1]
                telefono.telefono = tel.split(' ')[2]
            else:
                telefono.telefono = tel
            telefono.save()
            mensaje = 'Se guardo la informacion de Inscripcion, la administracion se comunicara prontamente con usted!'
            return render(request, 'mensaje.html', {'titulo': titulo, 'mensaje': mensaje, })
        else:
            message = form.errors
            return render(request, "extras/generic_form.html", {'message': message, 'form': form, 'titulo': 'Inscripcion de Socios', 'boton': 'Inscribite'})

def informar_pago(request):
    if request.method == "GET":
        form = PagoForm()
        return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Informe de Pago', 'boton': 'Guardar'})
    else:
        form = PagoForm(request.POST, request.FILES)
        if form.is_valid():
            titulo = 'Informar Pago'
            try:
                socio = Socio.objects.get(num_socio=form.cleaned_data['num_socio'], num_doc=form.cleaned_data['num_doc'])
                pago = form.save(commit=False)
                pago.socio = socio
                cuota = Cuota.objects.get(socio=socio, mes_cuota=form.cleaned_data['mes_cuota'], ano_cuota=form.cleaned_data['ano_cuota'])
                pago.cuota = cuota
                pago.save()
                mensaje = 'Se ha recibido la informacion del pago, la administracion le informara cuando este sea aprobado.'
            except Socio.DoesNotExist:
                mensaje = 'El Socio no existe.'
            except Cuota.DoesNotExist:
                mensaje = 'Usted no debe cuota para ese mes.'
            return render(request, 'mensaje.html', {'titulo': titulo, 'mensaje': mensaje, })
        else:
            message = form.errors
            return render(request, "extras/generic_form.html", {'message': message, 'form': form, 'titulo': 'Inscripcion de Socios', 'boton': 'Inscribite'})

@staff_member_required
def generar_cuotas(request, year=None, month=None):
    if request.method == "GET" and not year:
        form = MesForm()
        return render(request, 'extras/generic_form.html', {'form': form, 'titulo': 'Generacion de Cuotas', 'boton': 'Generar'})
    else:
        if year is None:
            form = MesForm(request.POST)
            if form.is_valid():
                #Generamos fecha de inicio y fin de la busqueda
                year = int(form.cleaned_data['year'])
                month = int(form.cleaned_data['month'])
            else:
                message = form.errors
                return render(request, "extras/generic_form.html", {'form': form, 'message': message, 'titulo': 'Generacion de Cuotas', 'boton': 'Generar'})
        #Buscamos si no hay cuotas ya creadas ese mes
        cuotas = Cuota.objects.filter(ano_cuota=year, mes_cuota=month, pagos=None)
        if cuotas:#Si tenemos cuotas ese mes, ofrecemos eliminarlas
            return render(request, "eliminar_cuotas.html", {'count': len(cuotas), 'periodo': str(year)+'/'+str(month) })
        else:
            cuotas = []#Vamos a generar cuotas para los socios activos
            for socio in Socio.objects.all():
                if socio.estado == 'AC':
                    if not socio.cuotas.filter(ano_cuota=year, mes_cuota=month):
                        cuota = Cuota()
                        cuota.socio = socio
                        cuota.ano_cuota = year
                        cuota.mes_cuota = month
                        cuota.descripcion = 'Cuota mensual por ' + socio.tipo_socio.nombre
                        cuota.valor = socio.tipo_socio.precio
                        for embarcacion in socio.embarcaciones.all():
                            cuota.descripcion+= ', incluye alojamiento ' + embarcacion.casco_marca + ' ' + embarcacion.casco_modelo + ' (' + embarcacion.tipo_alojamiento.nombre + ')'
                            cuota.valor+= embarcacion.tipo_alojamiento.precio
                        cuotas.append(cuota)
            Cuota.objects.bulk_create(cuotas)
            return render(request, 'cuotas_generadas.html', {'cuotas': cuotas})

@staff_member_required
def ver_cuotas(request, year, month):
    cuotas = Cuota.objects.filter(ano_cuota=year, mes_cuota=month)
    return render(request, 'cuotas_generadas.html', {'cuotas': cuotas})

@staff_member_required
def delete_cuotas(request, year, month):
    Cuota.objects.filter(ano_cuota=year, mes_cuota=month, pagos=None).delete()
    return generar_cuotas(request, year, month)

@staff_member_required
def deudores(request):
    deudores = []
    for socio in Socio.objects.all():
        for cuota in socio.cuotas.all():
            if not cuota.pagada()[0]:
                cuota.valor = cuota.pagada()[1]
                deudores.append(cuota)
    return render(request, 'deudores.html', {'cuotas': deudores})

@staff_member_required
def informe_facturacion(request):
    if request.method == "GET":
        form = PeriodoForm()
        return render(request, 'generic_form.html', {'form': form, 'titulo': 'Informe de Facturacion', 'boton': 'Buscar'})
    else:
        form = PeriodoForm(request.POST)
        if form.is_valid():
            cuotas = Cuota.objects.filter(
                (      
                    Q(ano_cuota__gt=form.cleaned_data['begda_year']) | (Q(ano_cuota=form.cleaned_data['begda_year']) & Q(mes_cuota__gte=form.cleaned_data['begda_month']))
                )
                    &
                (
                    Q(ano_cuota__lt=form.cleaned_data['endda_year']) | (Q(ano_cuota=form.cleaned_data['endda_year']) & Q(mes_cuota__lte=form.cleaned_data['endda_month']))
                )
            )
        ingresos = 0
        cuotas_pagadas = []
        adeudado = 0
        cuotas_adeudadas = []
        for cuota in cuotas:
            if cuota.pagada()[0]:
                cuotas_pagadas.append(cuota)
                ingresos+= cuota.valor
            else:
                if cuota.pagada()[1] != cuota.valor:
                    #Creamos parte adeudada
                    cuota_tmp = Cuota()
                    cuota_tmp.socio = cuota.socio
                    cuota_tmp.ano_cuota = cuota.ano_cuota
                    cuota_tmp.mes_cuota = cuota.mes_cuota
                    cuota_tmp.valor = cuota.pagada()[1]
                    cuota_tmp.parcial = True
                    cuotas_adeudadas.append(cuota_tmp)
                    adeudado+= cuota_tmp.valor
                    #Creamos parte cancelada
                    cuota.valor-= cuota.pagada()[1]
                    cuota.parcial = True
                    cuotas_pagadas.append(cuota)
                    ingresos+= cuota.valor
                else:
                    if cuota.pagos.all():
                        cuota.sin_verificar = True
                    cuotas_adeudadas.append(cuota)
                    adeudado+= cuota.pagada()[1]
        return render(request, 'facturacion.html', {'ingresos': ingresos, 'cuotas_pagadas': cuotas_pagadas,
            'adeudado': adeudado, 'cuotas_adeudadas': cuotas_adeudadas, })

#Upload Socios csv
@staff_member_required
def upload_socios(request):
    if request.method == "GET":
        form = UploadCsv()
        return render(request, "extras/upload_csv.html", {'titulo': 'Carga Masiva de Socios', 'form': form, })
    else:
        form = UploadCsv(request.POST, request.FILES)
        if form.is_valid():
            Socio.objects.all().delete()
            csv_file = form.cleaned_data['csvfile']
            lines = csv_file.read().decode("utf-8")
            lines = lines.split('\n')
            #Cargamos diccionarios
            dict_tiposocios = {ts.nombre : ts for ts in TipoSocio.objects.all()}
            dict_localidades = {l.nombre : l for l in Localidad.objects.all()}
            #Comenzamos la carga masiva
            for line in lines:
                line= line.split(';')
                #Campos del CSV
                #   0 Nsocio, 1 Apellidos, 2 Nombres, 
                #   3 Domicilio_Calle, 4 Domicilio_Numero, 5 Domicilio_Localidad, 
                #   6 Ndocumento,	7 Estado Civil, 8 Edad, 9 TipoSocio
                socio = Socio()
                socio.num_socio = line[0]
                socio.tipo_doc = 2
                if line[6]:
                    socio.num_doc = line[6]
                socio.apellidos = line[1]
                socio.nombres = line[2]
                socio.estado = 1
                socio.tipo_socio = dict_tiposocios[line[9]]
                if line[7] == 'Soltero':
                    socio.estado_civil = 1
                elif line[7] == 'Concubinato':
                    socio.estado_civil = 2
                elif line[7] == 'Casado':
                    socio.estado_civil = 3
                elif line[7] == 'Divorciado':
                    socio.estado_civil = 4
                elif line[7] == 'Viudo':
                    socio.estado_civil = 5
                elif line[7] == 'Sin Informar':
                    socio.estado_civil = 0
                if line[8]:
                    socio.fecha_nacimiento = datetime.today() - relativedelta(years=int(line[8]))
                socio.save()
                #Domicilio
                if line[3]:
                    domicilio = Domicilio()
                    domicilio.socio = socio
                    domicilio.localidad = dict_localidades[line[5]]
                    domicilio.calle = line[3]
                    domicilio.numero = line[4]
                    domicilio.save()
            return render(request, 'extras/upload_csv.html', {'count': len(lines), })
        else:
            message = form.errors
            return render(request, "extras/upload_csv.html", {'message': message, })