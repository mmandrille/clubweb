#Imports de python
import calendar
from datetime import date
#Imports django
from django.db.models import Q
#Import de proyecto
from finanzas.models import Asiento
#Imports de la app
from .models import Cuota, Pago

#Definimos nuestras funciones informativas
def vencida(pago):
    begda = date(pago.cuota.ano_cuota , pago.cuota.mes_cuota, 1)
    endda = date(begda.year, begda.month, calendar.monthrange(begda.year, begda.month)[1])
    if begda <= pago.fecha_pago and endda >= pago.fecha_pago:
        return False
    else:
        return True

def ingresos(begda, endda):
    movimentos = []
    pagos = Pago.objects.filter(fecha_pago__range=(begda, endda), aprobado=True)
    for pago in pagos:
        mov = Asiento()
        mov.tipo = 'IN'
        mov.origen = "Socios"
        mov.fecha = pago.fecha_pago
        mov.monto = pago.monto_pagado
        mov.descripcion = 'Cuota ' + str(pago.cuota.mes_cuota) + '/' + str(pago.cuota.ano_cuota) + ' de socio: ' + str(pago.cuota.socio.num_socio)
        movimentos.append(mov)
        #Si la cuota se pago fuera de tiempo
        if vencida(pago):#se debe generar descuento de tipo: Deudas por Cobrar (Negativo)
            mov = Asiento()
            mov.tipo = 'DC'
            mov.origen = "Socios"
            mov.fecha = pago.fecha_pago
            mov.monto = - pago.monto_pagado
            mov.descripcion = 'Cancelacion Deuda ' + str(pago.cuota.mes_cuota) + '/' + str(pago.cuota.ano_cuota) + ' de socio: ' + str(pago.cuota.socio.num_socio)
            movimentos.append(mov)
    return movimentos

def egresos(begda, endda):
    return []

def por_cobrar(begda, endda):
    movimentos = []
    cuotas = Cuota.objects.filter(
        (Q(ano_cuota__gt=begda.year) | (Q(ano_cuota=begda.year) & Q(mes_cuota__gte=begda.month)))
            &
        (Q(ano_cuota__lt=endda.year) | (Q(ano_cuota=endda.year) & Q(mes_cuota__lte=endda.month)))
    )
    for cuota in cuotas:
        if cuota.pagada()[1]:#Si queda deuda
            mov = Asiento()
            mov.tipo = 'DC'
            mov.origen = "Socios"
            mov.fecha = date(cuota.ano_cuota, cuota.mes_cuota, 1)
            mov.monto = cuota.pagada()[1]
            mov.descripcion = 'Adeudado por Cuota ' + str(cuota.mes_cuota) + '/' + str(cuota.ano_cuota) + ' de socio: ' + str(cuota.socio.num_socio)
            movimentos.append(mov)
    return movimentos

def por_pagar(begda, endda):
    return []

def recoleccion(begda, endda):
    movimientos = ingresos(begda, endda)
    movimientos+= egresos(begda, endda)
    movimientos+= por_cobrar(begda, endda)
    movimientos+= por_pagar(begda, endda)
    return movimientos