#Choice Fields
TIPO_DOCUMENTOS = (
    (1, 'CI'),
    (2, 'DNI'),
    (3, 'LC'),
    (4, 'LE'),
    (5, 'Pasaporte'),
)

ESTADO_CIVIL = (
    (0, 'Sin Informar'),
    (1, 'Soltero'),
    (2, 'Concubinato'),
    (3, 'Casado'),
    (4, 'Divorciado'),
    (5, 'Viudo'),
)

TIPO_EVENTO_SOCIO = (
    ('SE', 'Sin Estado'),
    ('IN', 'Inscripto'),
    ('AC', 'Activo'),
    ('FP', 'Inactivo por Falta de Pago'),
    ('BA', 'Dado de Baja')
)