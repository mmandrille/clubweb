#Imports Python
import random
#Imports Django
from django.test import TestCase
#Imports de la app
from .models import Socio, Pago, TipoPago

# Create your tests here.
def pagar_cuotas():
    Pago.objects.all().delete()
    socios = Socio.objects.all()
    pagos = []
    for socio in socios:
        if random.choice([True, True, True, True, True, True, False]):
            for cuota in  socio.cuotas.all():
                pago = Pago()
                pago.cuota = cuota
                pago.fecha_pago = str(cuota.ano_cuota) + '-' + str(cuota.mes_cuota) + '-' + random.choice(['9', '10', '11', '12', '13'])
                pago.tipo_pago = random.choice(TipoPago.objects.all())
                pago.monto_pagado = cuota.valor - random.choice([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 100])
                pago.aprobado = random.choice([True, True, True, True, True, True, True, False])
                pagos.append(pago)
    Pago.objects.bulk_create(pagos)