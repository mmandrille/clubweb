#Imports de python
from datetime import datetime
#Import de Django
from django.db import models
from django.contrib.auth.models import User
#Imports Extras
from tinymce.models import HTMLField
#Imports del proyecto
from georef.models import Localidad, Barrio
#Imports de la app
from .choices import TIPO_DOCUMENTOS, ESTADO_CIVIL, TIPO_EVENTO_SOCIO

# Create your models here.
class TipoSocio(models.Model):
    orden = models.SmallIntegerField(unique=True)
    nombre = models.CharField(verbose_name="Tipo de Socio", max_length=50)
    precio = models.DecimalField(verbose_name="Precio Mensual", max_digits=8, decimal_places=2)
    def __str__(self):
        return self.nombre

class TipoAlojamiento(models.Model):
    orden = models.SmallIntegerField(unique=True)
    nombre = models.CharField(verbose_name="Tipo de Alojamiento", max_length=50)
    precio = models.DecimalField(verbose_name="Precio Mensual", max_digits=8, decimal_places=2)
    def __str__(self):
        return self.nombre

class TipoPago(models.Model):
    nombre = models.CharField(verbose_name="Tipo de Pago", max_length=50)
    descripcion = HTMLField(blank=True, null=True)
    def __str__(self):
        return self.nombre

class Socio(models.Model):
    tipo_socio = models.ForeignKey(TipoSocio, on_delete=models.SET_NULL, related_name="socios", blank=True, null=True)
    num_socio = models.IntegerField(unique=True, null=True, blank=True)
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=2)
    num_doc = models.IntegerField('Num de Documento', unique=True, null=True, blank=True)
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento", null=True, blank=True)
    estado_civil = models.IntegerField(choices=ESTADO_CIVIL, default=2)
    email = models.EmailField('Correo Electronico', null=True, blank=True)
    fotografia = models.FileField('Fotografia', upload_to='socios/foto/', default='/static/img/noimage.png', blank=True, null=True)
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="socios")
    #Campos dinamicos
    def _estado(self):
        try:
            return self.eventos.order_by('fecha').last().tipo
        except self.DoesNotExist:
            return 'SE'
    estado = property(_estado)
    #Propiedades
    class Meta:
        ordering = ['num_socio']
   
    def __str__(self):
        return self.apellidos + ', ' + self.nombres

class EventoSocio(models.Model):
    socio = models.ForeignKey(Socio, on_delete=models.SET_NULL, null=True, related_name="eventos")
    tipo = models.CharField(max_length=2, choices=TIPO_EVENTO_SOCIO, default='SE')
    fecha = models.DateTimeField(verbose_name="Fecha del Evento", default=datetime.now)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    class Meta:
        ordering = ['fecha']
    def __str__(self):
        return str(self.socio) + ': ' + self.get_tipo_display() + ' el ' + str(self.fecha)
    def as_dict(self):
        return {
            "socio_id": self.socio.id,
            "tipo_evento": self.get_tipo_evento_display(),
            "fecha": str(self.fecha),
        }

class Domicilio(models.Model):
    socio = models.ForeignKey(Socio, on_delete=models.SET_NULL, null=True, related_name="domicilios_socio")
    localidad = models.ForeignKey(Localidad, on_delete=models.SET_NULL, related_name="domicilios_socio", blank=True, null=True)
    barrio = models.ForeignKey(Barrio, on_delete=models.SET_NULL, related_name="domicilios_socio", blank=True, null=True)
    calle = models.CharField('Calle', max_length=50, blank=True, null=True)
    numero = models.CharField('Numero', max_length=50, blank=True, null=True)

class Telefono(models.Model):
    socio = models.ForeignKey(Socio, on_delete=models.CASCADE, related_name="telefonos_socio")
    prefijo = models.CharField('Prefijo', max_length=5, blank=True, null=True, default="+54")
    cod_area = models.CharField('Codigo de Area', max_length=5, blank=True, null=True, default="342")
    telefono = models.CharField('Telefono', max_length=10, blank=True, null=True)
    def __str__(self):
        return self.prefijo + self.cod_area + self.telefono

class Embarcacion(models.Model):
    socio = models.ForeignKey(Socio, on_delete=models.CASCADE, related_name="embarcaciones")
    tipo_alojamiento = models.ForeignKey(TipoAlojamiento, on_delete=models.CASCADE, related_name="embarcaciones")
    casco_marca = models.CharField('Marca', max_length=20)
    casco_modelo = models.CharField('Modelo', max_length=50)
    casco_nombre = models.CharField('Nombre', max_length=50, blank=True, null=True)
    casco_matricula = models.CharField('Matricula', max_length=10, blank=True, null=True)
    motor_marca = models.CharField('Marca', max_length=20, blank=True, null=True)
    motor_modelo = models.CharField('Modelo', max_length=20, blank=True, null=True)
    motor_potencia = models.CharField('Potencia', max_length=10, blank=True, null=True)
    alojada = models.BooleanField(verbose_name="Alojada Actualmente", default=True)
    class Meta:
        verbose_name_plural = 'Embarcaciones'
    def __str__(self):
        return self.casco_marca + ' ' + self.casco_modelo + ' de ' + str(self.socio) + ' (' + (self.tipo_alojamiento.nombre) + ')'

#Pagos
class Cuota(models.Model):
    socio = models.ForeignKey(Socio, on_delete=models.CASCADE, related_name="cuotas")
    ano_cuota = models.IntegerField('Año', choices=[(y,y) for y in range(2019, datetime.today().year+1)], default=datetime.now().year,)
    mes_cuota = models.IntegerField('Mes', choices=[(m,m) for m in range(1,13)], default=datetime.now().month,)
    valor = models.DecimalField(verbose_name="Valor Mensual", max_digits=8, decimal_places=2, blank=True, null=True)
    descripcion = models.CharField('Descripcion', max_length=100, blank=True, null=True)
    class Meta:
        ordering = ['ano_cuota', 'mes_cuota']
    def __str__(self):
        return self.socio.apellidos + ', ' + self.socio.nombres + ' ' + str(self.mes_cuota) + '/' + str(self.ano_cuota)
    def pagada(self):
        importe = self.valor
        for pago in self.pagos.all().filter(aprobado=True):
            importe = importe - pago.monto_pagado
        if importe:
            return False, importe
        else:
            return True, 0
    def estado(self):
        if self.pagada()[0]:
            return 'Pagada'
        else:
            for pago in self.pagos.all():
                if pago.aprobado:
                    return 'Falta cancelar una parte'
                else:
                    return 'Aun no se ha aprobado el pago'
            return 'No se ha realizado pago'

class Pago(models.Model):
    cuota = models.ForeignKey(Cuota, on_delete=models.CASCADE, related_name="pagos")
    fecha_pago = models.DateField(verbose_name="Fecha del pago", blank=True, null=True)
    tipo_pago = models.ForeignKey(TipoPago, on_delete=models.CASCADE, related_name="pagos")
    monto_pagado = models.DecimalField(verbose_name="Monto Pagado", max_digits=8, decimal_places=2, blank=True, null=True)
    comprobante = models.FileField('comprobante', upload_to='socios/pagos/', blank=True, null=True)
    aprobado = models.BooleanField(verbose_name="Aprobado", default=False)

#Importamos señanesl
from .signals import nuevo_socio