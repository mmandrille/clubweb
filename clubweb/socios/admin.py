#Modulos Python
from datetime import datetime
#Modulos de Django
from django.db import models
from django.contrib import admin
from django.forms import TextInput
from django.shortcuts import render
from django.utils.safestring import mark_safe
#Modulos extras
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
#Importamos nuestros modelos
from .models import TipoSocio, TipoAlojamiento, TipoPago
from .models import Socio, EventoSocio, Domicilio, Telefono
from .models import Embarcacion
from .models import Cuota, Pago

#Models Ocultos
def register_hidden_models(*model_names):
    for m in model_names:
        ma = type(
            str(m)+'Admin',
            (admin.ModelAdmin,),
            {
                'get_model_perms': lambda self, request: {}
            })
        admin.site.register(m, ma)

#Definimos los inlines:
class DomicilioInline(admin.TabularInline):
    model = Domicilio
    fk_name = 'socio'
    autocomplete_fields = ['localidad', 'barrio']
    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.domicilios_socio.count():
            return 0
        return 1

class TelefonoInline(admin.TabularInline):
    model = Telefono
    fk_name = 'socio'
    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.telefonos_socio.count():
            return 0
        return 1

class EventoSocioInline(admin.TabularInline):
    model = EventoSocio
    fk_name = 'socio'
    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.eventos.count():
            return 0
        return 1

class PagoInline(NestedStackedInline):
    model = Pago
    fk_name = 'cuota'
    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.pagos.count():
            return 0
        return 1

class CuotaInline(NestedStackedInline):
    model = Cuota
    fk_name = 'socio'
    inlines = [PagoInline]
    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.cuotas.count():
            return 0
        return 1

#Definimos nuestros modelos administrables:
class SocioAdmin(NestedModelAdmin):
    model = Socio
    search_fields = ['num_doc', 'apellidos', 'nombres']
    list_filter = ['tipo_socio']
    inlines = [DomicilioInline, TelefonoInline, EventoSocioInline]# CuotaInline]
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
    }
    readonly_fields = ['resumen', 'estado']
    def resumen(self, obj):
        return mark_safe("<a href='/socios/" + str(obj.id) + "' target='_blank'>Click Aqui</a>")
    resumen.allow_tags = True
    fields = ['resumen', 'tipo_socio', ('num_socio', 'estado'), 
                ('tipo_doc', 'num_doc'), ('apellidos', 'nombres'),
                'fecha_nacimiento', 'email', 'fotografia',
                'usuario',]

class EmbarcacionAdmin(admin.ModelAdmin):
    model = Embarcacion
    search_fields = ['socio__num_doc', 'socio__apellidos', 'socio__nombres', 'casco_matricula']
    list_filter = ['tipo_alojamiento']
    autocomplete_fields = ['socio']
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
    }
    fieldsets = (
        ('Datos Basicos', {
            'fields': 
        ('socio','tipo_alojamiento', 'alojada')
        }),
        ('Casco', {
            'fields': ('casco_marca', 'casco_modelo', 'casco_nombre', 'casco_matricula')
        }),        
        ('Motor', {
            'fields': ('motor_marca', 'motor_modelo', 'motor_potencia')
        }),
    ) 
    def has_delete_permission(self, request, obj=None):
        return False

class CuotaAdmin(NestedModelAdmin):
    model = Cuota
    readonly_fields = ['socio']
    search_fields = ['socio__num_doc', 'socio__apellidos', 'socio__nombres']
    list_filter = ['socio__tipo_socio', 'pagos__aprobado', 'ano_cuota', 'mes_cuota']
    inlines = [PagoInline]
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
    }

#Registramos nuestros modelos
register_hidden_models(TipoSocio, TipoAlojamiento, TipoPago)
admin.site.register(Socio, SocioAdmin)
admin.site.register(Embarcacion, EmbarcacionAdmin)
admin.site.register(Cuota, CuotaAdmin)
