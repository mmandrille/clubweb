#Imports del Django
from django.apps import AppConfig

class SociosConfig(AppConfig):
    name = 'socios'
    def ready(self):
        #Agregamos al menu
        from core.apps import CoreConfig
        CoreConfig.ADMIN_MENU += [('Socios', self.name,)]
        try:#Agregamos a finanzas
            from finanzas.apps import FinanzasConfig
            from .integracion import recoleccion
            FinanzasConfig.APPS_FINANCIERAS += [(self.name, recoleccion)]
        except ModuleNotFoundError:
            print("No fue implementado el modulo de finanzas")
