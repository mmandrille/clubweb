#Imports de la app
from .models import Cuota

def periodos_generados():
    return Cuota.objects.all().values_list('ano_cuota', 'mes_cuota').distinct()